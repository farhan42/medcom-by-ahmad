import React from "react";
import { AuthContext, useAuthContext } from "./app/contexts";
import { AuthRoutes } from "./app/layouts/Auth";
import { MainRoutes } from "./app/layouts/Main";
import { Splash } from "./app/Views";

export const App = (): JSX.Element => {
  const context = useAuthContext();
  return (
    <div className="App">
      {context.isInitialized ? (
        context.isAuthenticated ? (
          <MainRoutes />
        ) : (
          <AuthRoutes />
        )
      ) : (
        <Splash />
      )}
    </div>
  );
};
