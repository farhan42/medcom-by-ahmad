import { IsNotEmpty, IsString } from "class-validator";

export class EditStaffValidator {
  @IsString()
  @IsNotEmpty({ message: "Invalid Name" })
  firstName: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Email Address" })
  email: string;

  // @IsString()
  // @IsNotEmpty({ message: "Invalid Password" })
  // password: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Role" })
  subRole: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Phone Number" })
  phoneNumber: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Address" })
  address: string;

  // @IsString()
  // @IsNotEmpty({ message: "Invalid Optional  Address" })
  // addressOptional: string;
}
