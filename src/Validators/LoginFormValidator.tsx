import { IsNotEmpty, IsString } from "class-validator";

export class LoginForm {
  @IsString()
  @IsNotEmpty({ message: "Invalid Identifier" })
  identifier: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Password" })
  password: string;

  realm;
}
