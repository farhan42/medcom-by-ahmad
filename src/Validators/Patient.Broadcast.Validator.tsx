import { IsNotEmpty, IsString } from "class-validator";

export class PatientBroadCastValidator {
  @IsString()
  @IsNotEmpty({ message: "Please Write Something" })
  message: string;
}
