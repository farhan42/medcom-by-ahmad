import { IsEmail, IsNotEmpty, IsPhoneNumber, IsString } from "class-validator";

export class RegisterForm {
  @IsString()
  @IsNotEmpty({ message: "Invalid Clinic Name" })
  title: string;

  @IsEmail()
  @IsNotEmpty({ message: "Invalid Email Address" })
  email: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Phone Number" })
  phoneNumber: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Password" })
  password: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Confirm Password" })
  confirmPassword: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Address" })
  address: string;
}
