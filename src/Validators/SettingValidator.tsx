import { IsNotEmpty, IsString } from "class-validator";

export class SettingValidator {
  @IsString()
  @IsNotEmpty({ message: "Invalid Clinic Name" })
  clinicname: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Password" })
  password: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Description" })
  description: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Email" })
  email: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Phone Number" })
  phoneNumber: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Tags" })
  tags: string;
}
