import { IsNotEmpty, IsString } from "class-validator";
import { FormEventHandler } from "react";

export class addNewStaffValidator {
  @IsString()
  @IsNotEmpty({ message: "Invalid Name" })
  firstName: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Email Address" })
  email: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Password" })
  password: string;

  @IsNotEmpty({ message: "Please select any Role" })
  subRole: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Phone Number" })
  phoneNumber: string;

  @IsString()
  @IsNotEmpty({ message: "Invalid Address" })
  address: string;

  clinicID;
  phoneNumberVerified;
  emailVerified;
  realm;
  role;
  twoStepEnabled;
}
