import { IsEmail, IsNotEmpty, IsString } from "class-validator";

export class forgotPasswordValidator {
  @IsString()
  @IsEmail()
  @IsNotEmpty({ message: "Invalid Email" })
  email: string;
}
