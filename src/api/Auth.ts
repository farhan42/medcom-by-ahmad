import { AxiosPromise } from "axios";
import { forgotPasswordInterface, ClinicInterface, UserInterface } from "./../interfaces";
import {
  forgotPasswordValidator,
  LoginForm,
  RegisterForm,
} from "./../Validators";
import { axiosInstance } from "./axiosinstance";

export class Auth {
  static Register(data: RegisterForm): AxiosPromise<ClinicInterface> {
    return axiosInstance({
      url: "/clinic/register",
      method: "Post",
      data,
    });
  }
  static Login(data: LoginForm): AxiosPromise<UserInterface> {
    return axiosInstance({
      url: "/user/login",
      method: "Post",
      data,
    });
  }
  static ForgotPassword(
    data: forgotPasswordValidator
  ): AxiosPromise<forgotPasswordInterface> {
    return axiosInstance({
      url: "/verification/email/initiate",
      method: "Post",
      data,
    });
  }
}
