import { axiosInstance } from "./axiosinstance";
import { AxiosPromise } from "axios";
import { MyNotificationInterface } from "../interfaces";
import { PatientBroadCastValidator } from "../Validators";

export class Broadcast {
  static create(
    data: PatientBroadCastValidator
  ): AxiosPromise<MyNotificationInterface> {
    return axiosInstance({
      url: "/broadcast",
      method: "post",
      data,
    });
  }
}
