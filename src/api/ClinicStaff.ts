import {
  UserFilterInterface,
  UserFilterResponseInterface,
  UserInterface,
} from "../interfaces";
import { AxiosPromise } from "axios";
import { addNewStaffValidator, EditStaffValidator } from "../Validators";
import { axiosInstance } from "./axiosinstance";

export class ClinicStaff {
  static create(data: addNewStaffValidator): AxiosPromise<UserInterface> {
    return axiosInstance({
      url: "/user",
      method: "post",
      data,
    });
  }

  static list(data: {
    filter: UserFilterInterface[];
  }): AxiosPromise<UserFilterResponseInterface[]> {
    return axiosInstance({
      url: "/user/filter",
      method: "post",
      data,
    });
  }

  static getById(id: string): AxiosPromise<UserInterface> {
    return axiosInstance({
      url: `/clinic/${id}`,
      method: "get",
    });
  }

  static updateById(
    id: string,
    data: EditStaffValidator
  ): AxiosPromise<UserInterface> {
    return axiosInstance({
      url: `/clinic/${id}`,
      method: "patch",
      data,
    });
  }
  static deleteById(id: string): AxiosPromise<UserInterface> {
    return axiosInstance({
      url: `/user/${id}`,
      method: "delete",
    });
  }
}
