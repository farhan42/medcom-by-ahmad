import { AxiosPromise } from "axios";
import { ClinicInterface } from "../interfaces";
import { SettingValidator } from "../Validators";
import { axiosInstance } from "./axiosinstance";

export class ClinicUpdate {
  static updateById(
    id: string,
    data: SettingValidator
  ): AxiosPromise<ClinicInterface> {
    return axiosInstance({
      url: `/user/${id}`,
      method: "patch",
      data,
    });
  }
}
