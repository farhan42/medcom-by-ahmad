import React from "react";
import { axiosInstance } from "./axiosinstance";
import { AxiosPromise } from "axios";
import { MediaUploadInterface } from "../interfaces";
export class Media extends React.Component {
  static upload(
    file: File,
    onUploadProgress?: (ProgressEvent: ProgressEvent) => void
  ): AxiosPromise<MediaUploadInterface> {
    const formData = new FormData();
    formData.set("file", file);
    return axiosInstance({
      url: "/media/upload",
      method: "post",
      data: formData,
      onUploadProgress,
    });
  }

  static getFileByName(filename: string): string {
    const url = new URL("http://localhost:3000/");
    url.pathname = `/media/${filename}`;
    return url.href;
  }
}
