import { axiosInstance } from "./axiosinstance";
import { AxiosPromise } from "axios";
import { MyNotificationInterface } from "../interfaces";

export class Notification {
  static list(data): AxiosPromise<MyNotificationInterface[]> {
    return axiosInstance({
      url: "/notification/myNotifications",
      method: "get",
      data,
    });
  }
}
