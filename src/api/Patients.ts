import { AxiosPromise } from "axios";
import { PatientInteface, UserFilterInterface, UserFilterResponseInterface } from "../interfaces";
import { axiosInstance } from "./axiosinstance";

export class Patients {
  static list(): AxiosPromise<PatientInteface[]> {
    return axiosInstance({
      url: "/patient-enrolment/clinic/patients",
      method: "get",
    });
  }
  static filter(data: {
    pfilter: UserFilterInterface[];
  }): AxiosPromise<UserFilterResponseInterface[]> {
    return axiosInstance({
      url: "/patient-enrolment/filter",
      method: "post",
      data,
    });
  }
  static deleteById(id: string): AxiosPromise<PatientInteface> {
    return axiosInstance({
      url: `/patient-enrolment/${id}`,
      method: "delete",
    });
  }
}
