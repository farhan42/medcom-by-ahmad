import { ClinicUpdate } from "./ClinicUpdate";
import { Broadcast } from "./Broadcast";
import { Notification } from "./Notification";
import { Media } from "./Media";
import { Auth } from "./Auth";
import { ClinicStaff } from "./ClinicStaff";
import { Patients } from "./Patients";

export const API = {
  Auth,
  ClinicStaff,
  Patients,
  Notification,
  Media,
  Broadcast,
  ClinicUpdate,
};
