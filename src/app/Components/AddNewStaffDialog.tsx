import { useFormik } from "formik";
import React from "react";
import { Modal } from "react-bootstrap";
import { API } from "../../api";

import add_staff_close_btn from "../../assets/icons/add_staff_close.svg";
import { UserRealmEnum, UserRoleEnum, UserSubRoleEnum } from "../../interfaces";
import { FormikValidator } from "../../shared/utility";
import { addNewStaffValidator } from "../../Validators";
import { useAuthContext } from "../contexts";
import { FormikErrorMessage } from "./FormikErrorMessage";

interface Props {
  show: boolean;
  onClose: () => void;
}

export const AddNewStaffDialog = (props: Props): JSX.Element => {
  const context = useAuthContext();
  const addNewStaffFormik = useFormik<addNewStaffValidator>({
    initialValues: {
      firstName: "",
      email: "",
      password: "",
      subRole: null,
      phoneNumber: "",
      address: "",

      clinicID: context.user.clinicID,
      phoneNumberVerified: false,
      emailVerified: false,
      realm: UserRealmEnum.Clinic,
      role: UserRoleEnum.User,
      twoStepEnabled: false,
    },
    onSubmit: (values) => {
      API.ClinicStaff.create(values)
        .then((response) => {
          console.log("Success!");
          props.onClose();
        })
        .catch((error) => {
          console.error(error);
        });
    },
    validate: FormikValidator.validator(addNewStaffValidator),
  });

  return (
    <Modal
      className="add_new_staff Modal"
      show={props.show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <form
        onSubmit={addNewStaffFormik.handleSubmit}
        className="add_new_staff_container"
      >
        <div className="add_new_staff_head">
          <h1>Add New Staff</h1>
          <img onClick={props.onClose} src={add_staff_close_btn} alt="" />
        </div>
        <div className="add_new_staff_input_fields">
          <div className="new_staff_input_fields">
            <div className="left_input_fields">
              <div className="left_input_field">
                <label htmlFor="firstName"> Name </label>
                <input
                  name="firstName"
                  id="firstName"
                  type="text"
                  className="input"
                  placeholder="Clinic Name"
                  onBlur={addNewStaffFormik.handleBlur}
                  onChange={addNewStaffFormik.handleChange}
                />
                <FormikErrorMessage
                  formik={addNewStaffFormik}
                  name="firstName"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="left_input_field">
                <label htmlFor="email"> Email Address </label>
                <input
                  name="email"
                  id="email"
                  type="email"
                  className="input"
                  placeholder="Email Address"
                  onBlur={addNewStaffFormik.handleBlur}
                  onChange={addNewStaffFormik.handleChange}
                />
                <FormikErrorMessage
                  formik={addNewStaffFormik}
                  name="email"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="left_input_field">
                <label htmlFor="password"> Password </label>
                <input
                  name="password"
                  id="password"
                  type="Password"
                  className="input"
                  placeholder="Password"
                  onBlur={addNewStaffFormik.handleBlur}
                  onChange={addNewStaffFormik.handleChange}
                />
                <FormikErrorMessage
                  formik={addNewStaffFormik}
                  name="password"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
            </div>

            <div className="right_input_fields">
              <div className="right_input_field">
                <label htmlFor="subRole">SubRole </label>
                <select
                  onChange={addNewStaffFormik.handleChange}
                  // onBlur={addNewStaffFormik.handleBlur}
                  name="subRole"
                  id="subRole"
                >
                  <option value={null} disabled selected={true}> Select </option>
                  <option value={UserSubRoleEnum.Admin}>Admin</option>
                  <option value={UserSubRoleEnum.AdminStaff}> Admin Staff </option>
                  <option value={UserSubRoleEnum.Doctor}> Doctor</option>
                  <option value={UserSubRoleEnum.DoctorAdmin}> Admin Doctor
                  </option>
                  <option value={UserSubRoleEnum.Staff}>Staff</option>
                </select>
                <FormikErrorMessage
                  formik={addNewStaffFormik}
                  name="subRole"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="right_input_field">
                <label htmlFor="phoneNumber"> Phone Number </label>
                <input
                  name="phoneNumber"
                  id="phoneNumber"
                  type="tel"
                  className="input"
                  placeholder="Phone Number"
                  onBlur={addNewStaffFormik.handleBlur}
                  onChange={addNewStaffFormik.handleChange}
                />
                <FormikErrorMessage
                  formik={addNewStaffFormik}
                  name="phoneNumber"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="right_input_field">
                <label htmlFor="address"> Address </label>
                <input
                  name="address"
                  id="address"
                  type="text"
                  className="input"
                  placeholder="Address"
                  onBlur={addNewStaffFormik.handleBlur}
                  onChange={addNewStaffFormik.handleChange}
                />
                <FormikErrorMessage
                  formik={addNewStaffFormik}
                  name="address"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="done_add_btn">
          <button type="submit" className="a_d_button">
            Add Staff
          </button>
        </div>
      </form>
    </Modal>
  );
};
