import { useFormik } from "formik";
import React from "react";
import { Modal } from "react-bootstrap";
import { API } from "../../api";

import broadcast_close_btn from "../../assets/icons/add_staff_close.svg";
import { FormikValidator } from "../../shared/utility";
import { PatientBroadCastValidator } from "../../Validators";
import { FormikErrorMessage } from "./FormikErrorMessage";

interface Props {
  show: boolean;
  onClose: () => void;
}

export const BroadcastDialog = (props: Props): JSX.Element => {
  const broadcastFormik = useFormik<PatientBroadCastValidator>({
    initialValues: {
      message: "",

      // text: "",
      // enrolments: ""[],
    },
    onSubmit: (values) => {
      API.Broadcast.create(values)
        .then((response) => {
          console.log(response);
          props.onClose();
        })
        .catch((error) => {
          console.log(error);
        });
    },
    validate: FormikValidator.validator(PatientBroadCastValidator),
  });

  return (
    <Modal
      className="broadcast_message_dialog"
      show={props.show}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <form
        onSubmit={broadcastFormik.handleSubmit}
        className="delete_staff_dialog_container"
      >
        <div className="broadcast_head">
          <h1>Broadcast Message</h1>
          <img onClick={props.onClose} src={broadcast_close_btn} alt="" />
        </div>
        <div className="broadcast_message">
          <label htmlFor="message">Message</label>
          <textarea
            name="message"
            id="message"
            rows={5}
            placeholder="Message"
            onChange={broadcastFormik.handleChange}
            onBlur={broadcastFormik.handleBlur}
          ></textarea>
          <FormikErrorMessage
            formik={broadcastFormik}
            name="message"
            render={(error) => <span className="error">{error}</span>}
          />
        </div>
        <div className="broadcast_dialog_btn">
          <button type="submit" className="btn">
            {" "}
            Send{" "}
          </button>
        </div>
      </form>
    </Modal>
  );
};
