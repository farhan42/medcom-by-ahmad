import React from "react";
import { Modal } from "react-bootstrap";

import color_dialog_remove from "../../assets/icons/cross.svg";
interface Props {
  show: boolean;
  onClose: () => void;
}

export const ChangeThemeColorDialog = (props: Props): JSX.Element => {
  return (
    <Modal
      className="change_theme_color modal"
      size="lg"
      show={props.show}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <div className="change_theme_color_container">
        <div className="change_theme_color_head">
          <h1>Change Theme Colors</h1>
          <img src={color_dialog_remove} alt="" onClick={props.onClose} />
        </div>
        <div className="b_themes">
          <div className="left_themes">
            <div className="theme ">
              <label htmlFor="color"> Primary: </label>
              <div className="color_hex primary">
                <input type="color" name="color" id="color" />
                <p>#4282C5</p>
              </div>
            </div>

            <div className="theme ">
              <label htmlFor="color"> Primary Light: </label>
              <div className="color_hex primary_light">
                <input type="color" name="color" id="color" />
                <p>#ECF3F9</p>
              </div>
            </div>

            <div className="theme ">
              <label htmlFor="color"> Text Dark: </label>
              <div className="color_hex text_dark">
                <input type="color" name="color" id="color" />
                <p>#02100F</p>
              </div>
            </div>

            <div className="theme">
              <label htmlFor="color"> Text Medium: </label>
              <div className="color_hex text_medium">
                <input type="color" name="color" id="color" />
                <p>#9A9F9F</p>
              </div>
            </div>

            <div className="theme ">
              <label htmlFor="color"> Text Light: </label>
              <div className="color_hex text_light">
                <input type="color" name="color" id="color" />
                <p>#FFFFFF</p>
              </div>
            </div>
          </div>

          <div className="right_themes">
            <div className="theme ">
              <label htmlFor="color"> Success: </label>
              <div className="color_hex success">
                <input type="color" name="color" id="color" />
                <p>#2BBD77</p>
              </div>
            </div>

            <div className="theme ">
              <label htmlFor="color"> Danger:: </label>
              <div className="color_hex danger">
                <input type="color" name="color" id="color" />
                <p>#F52100</p>
              </div>
            </div>

            <div className="theme ">
              <label htmlFor="color"> Warning: </label>
              <div className="color_hex warning">
                <input type="color" name="color" id="color" />
                <p>#E8910F</p>
              </div>
            </div>

            <div className="theme ">
              <label htmlFor="color"> Background: </label>
              <div className="color_hex background">
                <input type="color" name="color" id="color" />
                <p>#FAFAFA</p>
              </div>
            </div>
          </div>
        </div>

        <div className="theme_btns">
          <button type="button" className="save" onClick={props.onClose}>
            Save
          </button>
          <button type="button" className="reset" onClick={props.onClose}>
            Reset
          </button>
        </div>
      </div>
    </Modal>
  );
};
