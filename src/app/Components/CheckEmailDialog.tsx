import React from "react";
import { Modal } from "react-bootstrap";

import checkEmailCross from "../../assets/icons/cross.svg";
import emailNotification from "../../assets/Images/check_email.png";

// const  checkEmailCross = require("../../assets/icons/cross.svg");
// const emailNotification = require("../../../assets/Images/check_email.png");

interface Props {
  show: boolean;
  onclose: () => void;
}

export const CheckEmailDialog = (props: Props) => {
  return (
    <>
      <Modal
        className="forgot_password_modal modal"
        show={props.show}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <div className="check_email">
          <div className="check_email_img">
            <div className="check_email_cross_icon">
              <img onClick={props.onclose} src={checkEmailCross} alt="" />
            </div>
            <img src={emailNotification} alt="" />
          </div>
          <div className="check_email_detail">
            <h1>Check Email</h1>
            <p>
              Amet minim mollit non deserunt ullamco est sit aliqua dolor do
              amet sint. Velit officia consequat duis.
            </p>
          </div>
        </div>
      </Modal>
    </>
  );
};
