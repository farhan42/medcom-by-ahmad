import React from "react";
import { Modal } from "react-bootstrap";

import search_icon from "../../assets/icons/search.svg";
import c_l_cross from "../../assets/icons/cross.svg";
import c_l_btn from "../../assets/icons/c_l_btn.svg";

const clinic_change_location = require("../../assets/Images/clinic_location.png");

interface Props {
  show: boolean;
  onClose: () => void;
}

export const ClinicLocationDialog = (props: Props): JSX.Element => {
  return (
    <Modal
      className="change_your_clinic_location modal"
      size="lg"
      show={props.show}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <div className="change_location_container">
        <div className="c_l_head">
          <h1>Change your Clinic Location</h1>
          <div className="c_l_search_patient">
            <input
              type="text"
              name="search"
              id="search"
              className="input"
              placeholder="Enter your Clinic address here"
            />
            <img src={search_icon} alt="" />
          </div>
          <img
            className="c_cross"
            src={c_l_cross}
            alt=""
            onClick={props.onClose}
          />
        </div>
        <div className="c_l_img">
          <img className="bg_img" src={clinic_change_location} alt="" />
          <div className="c_l_zooming_btns">
            <img className="c_location" src={c_l_btn} alt="" />
            <button className="c_l_zoom_in" type="button">
              +
            </button>
            <button className="c_l_zoom_out" type="button">
              -
            </button>
          </div>
        </div>
        <div className="save_c_l_btn">
          <button className="c_L_btn" type="button" onClick={props.onClose}>
            Save Your Clinic Location
          </button>
        </div>
      </div>
    </Modal>
  );
};
