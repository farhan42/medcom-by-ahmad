import React, { useEffect } from "react";
import { Modal } from "react-bootstrap";

import delete_staff_cross_icon from "../../assets/icons/cross.svg";
import { useDispatch } from "../../store";
import { deleteStaffThunk } from "../../thunks";

const delete_staff_img = require("../../assets/Images/delete_staff.png");

interface Props {
  id: string;
  show: boolean;
  onClose: () => void;
}

export const DeleteStaffDialog = (props: Props): JSX.Element => {
  const dispatch = useDispatch();

  const handleConfirmDelete = () => {
    dispatch(deleteStaffThunk(props.id));
    props.onClose();
  };
  useEffect(() => {
    console.log("deleted item", props.id);
  }, []);
  return (
    <Modal
      className="delete_staff_dialog"
      show={props.show}
      //   size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <div className="delete_staff_dialog_container">
        <div className="delete_staff_dialog_img">
          <img src={delete_staff_img} alt="" />
          <img
            onClick={props.onClose}
            className="close_delete_dialog"
            src={delete_staff_cross_icon}
            alt=""
          />
        </div>
        <div className="delete_staff_dialog_name">
          <h1>Are you sure you want to delete Eli Dotson?</h1>
        </div>
        <div className="delete_staff_dialog_btns">
          <button
            type="button"
            className="not_sure"
            onClick={handleConfirmDelete}
          >
            Yes I'm sure
          </button>
          <button type="button" className="sure" onClick={props.onClose}>
            No I'm not sure
          </button>
        </div>
      </div>
    </Modal>
  );
};
