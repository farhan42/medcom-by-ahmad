import { Formik, useFormik } from "formik";
import React, { useEffect } from "react";
import { Modal } from "react-bootstrap";
import { FormikErrorMessage } from ".";
import { API } from "../../api";

import add_staff_close_btn from "../../assets/icons/add_staff_close.svg";
import { UserInterface, UserSubRoleEnum } from "../../interfaces";
import { FormikValidator } from "../../shared/utility";
import { EditStaffValidator } from "../../Validators";

interface Props {
  user: UserInterface;
  show: boolean;
  onClose: () => void;
}

export const EditStaffDialog = (props: Props): JSX.Element => {
  const user = props.user;
  const EditStaffFormik = useFormik<EditStaffValidator>({
    initialValues: {
      // firstName: "",
      // email: "",
      // password: ""
      // subRole: "",
      // phoneNumber: "",
      // address: "",
      // addressOptional: "",

      firstName: user.firstName,
      email: user.email,
      subRole: user.subRole,
      phoneNumber: user.phoneNumber,
      address: user.address,
    },
    onSubmit: (values) => {
      API.ClinicStaff.updateById(user.id, values)
        .then((response) => {
          console.log(response);
          props.onClose();
        })
        .catch((error) => {
          console.error(error);
        });
    },
    validate: FormikValidator.validator(EditStaffValidator),
  });

  // useEffect(() => {
  //   console.log("User from Props =>", user);
  // }, []);

  return (
    <Modal
      className="add_new_staff Modal"
      show={props.show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <form
        onSubmit={EditStaffFormik.handleSubmit}
        className="add_new_staff_container"
      >
        <div className="add_new_staff_head">
          <h1>Edit Staff</h1>
          <img onClick={props.onClose} src={add_staff_close_btn} alt="" />
        </div>
        <div className="add_new_staff_input_fields">
          <div className="new_staff_input_fields">
            <div className="left_input_fields">
              <div className="left_input_field">
                <label htmlFor="firstName"> Name </label>
                <input
                  name="firstName"
                  id="firstName"
                  type="text"
                  className="input"
                  placeholder="Name"
                  value={EditStaffFormik.values.firstName}
                  onChange={EditStaffFormik.handleChange}
                  onBlur={EditStaffFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={EditStaffFormik}
                  name="firstName"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>

              <div className="left_input_field">
                <label htmlFor="email"> Email Address </label>
                <input
                  name="email"
                  id="email"
                  type="email"
                  className="input"
                  placeholder="Email Address"
                  value={EditStaffFormik.values.email}
                  onChange={EditStaffFormik.handleChange}
                  onBlur={EditStaffFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={EditStaffFormik}
                  name="email"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              {/* <div className="left_input_field">
                <label htmlFor="password"> Password </label>
                <input
                  name="password"
                  id="password"
                  type="Password"
                  className="input"
                  placeholder="Password"
                  value={EditStaffFormik.values.password}
                  onChange={EditStaffFormik.handleChange}
                  onBlur={EditStaffFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={EditStaffFormik}
                  name="password"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div> */}
            </div>

            <div className="right_input_fields">
              <div className="right_input_field">
                <label htmlFor="subRole">SubRole </label>
                <select
                  onChange={EditStaffFormik.handleChange}
                  // onBlur={addNewStaffFormik.handleBlur}
                  name="subRole"
                  id="subRole"
                >
                  <option value={null} disabled selected={true}> Select </option>
                  <option value={UserSubRoleEnum.Admin}>Admin</option>
                  <option value={UserSubRoleEnum.AdminStaff}> Admin Staff </option>
                  <option value={UserSubRoleEnum.Doctor}> Doctor</option>
                  <option value={UserSubRoleEnum.DoctorAdmin}> Admin Doctor </option>
                  <option value={UserSubRoleEnum.Staff}>Staff</option>
                  {/* value={EditStaffFormik.values.subRole} */}
                </select>
                <FormikErrorMessage
                  formik={EditStaffFormik}
                  name="subRole"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="right_input_field">
                <label htmlFor="phoneNumber"> Phone Number </label>
                <input
                  name="phoneNumber"
                  id="phoneNumber"
                  type="tel"
                  className="input"
                  placeholder="Phone Number"
                  value={EditStaffFormik.values.phoneNumber}
                  onChange={EditStaffFormik.handleChange}
                  onBlur={EditStaffFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={EditStaffFormik}
                  name="phoneNumber"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="right_input_field">
                <label htmlFor="address"> Address </label>
                <input
                  name="address"
                  id="address"
                  type="text"
                  className="input"
                  placeholder="Address"
                  value={EditStaffFormik.values.address}
                  onChange={EditStaffFormik.handleChange}
                  onBlur={EditStaffFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={EditStaffFormik}
                  name="address"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
            </div>
          </div>

          {/* <div className="optional">
            <label htmlFor="addressOptional">
              {" "}
              Address <span> (Optional) </span>{" "}
            </label>
            <input
              name="addressOptional"
              id="addressOptional"
              type="text"
              className="input"
              placeholder="Address"
              onChange={EditStaffFormik.handleChange}
              onBlur={EditStaffFormik.handleBlur}
            />
            <FormikErrorMessage
              formik={EditStaffFormik}
              name="addressOptional"
              render={(error) => <span className="error">{error}</span>}
            />
          </div> */}
        </div>

        <div className="done_add_btn">
          <button type="submit" className="a_d_button">
            Save Changes
          </button>
        </div>
      </form>
    </Modal>
  );
};
