import { useFormik } from "formik";
import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import { API } from "../../api";

import crossIcon from "../../assets/icons/cross.svg";
import { FormikValidator } from "../../shared/utility";
import { forgotPasswordValidator } from "../../Validators";
import { FormikErrorMessage } from "./FormikErrorMessage";

interface Props {
  show: boolean;
  onClose: () => void;
  onHide: () => void;
}

export const ForgotPasswordDialog = (props: Props): JSX.Element => {
  const forgotPasswordForm = useFormik<forgotPasswordValidator>({
    initialValues: {
      email: "",
    },
    onSubmit: (values) => {
      API.Auth.ForgotPassword(values)
        .then((response) => {
          console.log(response);
          props.onClose();
        })
        .catch((error) => {
          console.log(error);
        });
    },
    validate: FormikValidator.validator(forgotPasswordValidator),
  });

  return (
    <>
      <Modal
        className="forgot_password_modal Modal"
        show={props.show}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <form onSubmit={forgotPasswordForm.handleSubmit}>
          <div className="forgot_password_head">
            <h1> Forgot Password? </h1>
            <img onClick={props.onHide} src={crossIcon} alt="" />
          </div>
          <div className="modal_input_field">
            <label htmlFor="email">Email Address</label>
            <input
              className="input"
              type="email"
              name="email"
              id="email"
              placeholder="Email Address"
              onChange={forgotPasswordForm.handleChange}
              onBlur={forgotPasswordForm.handleBlur}
            />
            <FormikErrorMessage
              formik={forgotPasswordForm}
              name="email"
              render={(error) => <span className="error">{error}</span>}
            />
          </div>
          <button className="send_email" type="submit">
            Send Email
          </button>
        </form>
      </Modal>
    </>
  );
};
