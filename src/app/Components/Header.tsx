import React, { useEffect } from "react";

import approved_icon from "../../assets/icons/approved.svg";
import approved_drop_icon from "../../assets/icons/approved_drop.svg";
import search_icon from "../../assets/icons/search.svg";
import notification_bell from "../../assets/icons/notification.svg";
import setting_down_icon from "../../assets/icons/setting_drop.svg";
import { Dropdown } from "react-bootstrap";
import { NavLink, useNavigate } from "react-router-dom";
import { useAuthContext } from "../contexts";
import { useDispatch } from "../../store";
import { NotificationListThunk } from "../../thunks";

const user_image = require("../../assets/Images/butler.png");
const eli_doston_image = require("../../assets/Images/eli_doston.png");
const juniper_wolf_image = require("../../assets/Images/juniper_wolf.png");

export const Header = (): JSX.Element => {
  const context = useAuthContext();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const hanldeDropdownLogout = () => {
    context.updateUser(null);
    navigate("login");
  };

  const hanldeSetting = () => {
    navigate("setting");
  };

  useEffect(() => {
    dispatch(NotificationListThunk());
  });

  return (
    <div className="my_header">
      <div className="header_container">
        <div className="header_types">
          <div className="approved_patient_list">
            <Dropdown align="end">
              <Dropdown.Toggle className="patient_list_dropdown">
                <div className="approved">
                  <img src={approved_icon} alt="" />
                  <p>Approved</p>
                </div>
                <div className="approved_drop">
                  <img src={approved_drop_icon} alt="" />
                </div>
              </Dropdown.Toggle>
              <Dropdown.Menu className="patient_dropdown_menu">
                <Dropdown.Item className="all">All</Dropdown.Item>
                <Dropdown.Item className="approved">Approved</Dropdown.Item>
                <Dropdown.Item className="not_approved">Not Approved</Dropdown.Item>
                <div className="p_top_face"></div>
              </Dropdown.Menu>
            </Dropdown>
          </div>
          <div className="all_patient_list">
            <Dropdown align="end">
              <Dropdown.Toggle className="all_patient_list_dropdown">
                <div className="all_default">
                  <img src={approved_icon} alt="" />
                  <p>All</p>
                </div>
                <div className="all_drop">
                  <img src={approved_drop_icon} alt="" />
                </div>
              </Dropdown.Toggle>
              <Dropdown.Menu className="all_patient_dropdown_menu">
                <Dropdown.Item className="all">All</Dropdown.Item>
                <Dropdown.Item className="approved">Approved</Dropdown.Item>
                <Dropdown.Item className="not_approved">Not Approved</Dropdown.Item>
                <div className="a_d_top_face"></div>
              </Dropdown.Menu>
            </Dropdown>
          </div>
          <div className="search_patient">
            <input
              type="text"
              name="search"
              id="search"
              className="input"
              placeholder="Search..."
            />
            <img src={search_icon} alt="" />
          </div>
          <div className="patient_notification">
            <Dropdown align="end">
              <Dropdown.Toggle className="notification_dropdown">
                <img src={notification_bell} alt="" />
              </Dropdown.Toggle>
              <Dropdown.Menu className="d_menu">
                <div className="anne_container">
                  <div className="anne_head">
                    <h1>Notification</h1>
                  </div>
                  <div className="requests">
                    <div className="request">
                      <div className="anne_detail">
                        <div className="anne_img">
                          <img src={user_image} alt="" />
                        </div>
                        <div className="anne_name">
                          <h1>Anne Butler</h1>
                          <h6>New patient joined the clinic</h6>
                        </div>
                      </div>
                      <div className="status">
                        <button type="button" className="user_status">See Details</button>
                      </div>
                    </div>

                    <div className="request">
                      <div className="anne_detail">
                        <div className="anne_img">
                          <img src={juniper_wolf_image} alt="" />
                        </div>
                        <div className="anne_name">
                          <h1>MEDCOM</h1>
                          <h6>New update available for you</h6>
                        </div>
                      </div>
                      <div className="status">
                        <button type="button" className="user_status">Update Now</button>
                      </div>
                    </div>

                    <div className="request">
                      <div className="anne_detail">
                        <div className="anne_img">
                          <img src={eli_doston_image} alt="" />
                        </div>
                        <div className="anne_name">
                          <h1>Eli Dotson</h1>
                          <h6>I need a new appointment</h6>
                        </div>
                      </div>
                      <div className="status">
                        <button type="button" className="user_status">See Chat</button>
                      </div>
                    </div>

                    <div className="request">
                      <div className="anne_detail">
                        <div className="anne_img">
                          <img src={juniper_wolf_image} alt="" />
                        </div>
                        <div className="anne_name">
                          <h1>Juniper Wolf</h1>
                          <h6>New staff memeber added</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="all_notification">
                    <NavLink to="/all_notification">
                      See All Notifaction
                    </NavLink>
                  </div>
                  <div className="rotate"></div>
                </div>
                <div className="top_face"></div>
              </Dropdown.Menu>
            </Dropdown>
            <p></p>
          </div>
          <div className="user_account">
            <div className="user_img">
              <img src={user_image} alt="" />
            </div>
            <div className="user_detail">
              <div className="user_name">
                <h1>
                  {context.user.firstName} {context.user.lastName}
                </h1>
              </div>
              <div className="user_setting">
                <Dropdown align="end">
                  <Dropdown.Toggle className="user_dropdown">
                    Setting <img src={setting_down_icon} alt="" />
                  </Dropdown.Toggle>
                  <Dropdown.Menu className="user_dropdown_menu">
                    <Dropdown.Item className="profile" onClick={hanldeSetting}>
                      Profile
                    </Dropdown.Item>
                    <Dropdown.Item
                      className="logout"
                      onClick={hanldeDropdownLogout}
                    >
                      Logout
                    </Dropdown.Item>
                    <div className="u_s_top_face"></div>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
