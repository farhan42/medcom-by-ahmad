// import React, { useState } from "react";
// import { NavLink } from "react-router-dom";
// import { Dropdown } from "react-bootstrap";
// import { ReferDialog } from "./ReferDialog";
// import { ChatAccessDialog } from "./ChatAccessDialog";

// import internal_down_icon from "../../assets/icons/internal_down.svg";
// import john_chat_detail_icon from "../../assets/icons/chat_client_detail.svg";
// import john_chat_screen_zoom_out_icon from "../../assets/icons/chat_client_screen_zoom_out.svg";
// import john_chat_screen_zoom_in_icon from "../../assets/icons/chat_client_screen_zoom_in.svg";
// import john_chat_dialogs_icon from "../../assets/icons/chat_client_dialogs_icon.svg";
// import chat_lock_icon from "../../assets/icons/chat_lock.svg";
// import attach_file_icon from "../../assets/icons/attach_file.svg";
// import send_sms_icon from "../../assets/icons/send_sms.svg";
// import appointment_pin_icon from "../../assets/icons/pin_appointment.svg";

// const admin_img = require("../../assets/Images/admin.png");
// const doctor_img = require("../../assets/Images/doctor.png");
// const chat_client_img = require("../../assets/Images/chat_client.png");
// const send_img_1 = require("../../assets/Images/send_img.png");
// const send_img_2 = require("../../assets/Images/send_img_2.png");
// const send_img_3 = require("../../assets/Images/send_img_3.png");

// export const JoshChat = (): JSX.Element => {
//   const [referDialog, setReferDialog] = useState<boolean>(false);
//   const [chatAccessDialog, setChatAccessDialog] = useState<boolean>(false);

//   const handleShowReferDialog = () => {
//     setReferDialog(true);
//   };

//   const handleCloseReferDialog = () => {
//     setReferDialog(false);
//   };

//   const handleShowChatAccessDialog = () => {
//     setChatAccessDialog(true);
//   };

//   const handleCloseChatAccessDialog = () => {
//     setChatAccessDialog(false);
//   };

//   return (
//     <>
//       <div className="medcom_chat josh_chat_main">
//         <div className="chat_container josh_chat_container">
//           <div className="chat_list">
//             <div className="chat_list_head">
//               <h1>Chat</h1>
//             </div>
//             <div className="internal_chat_list">
//               <div className="internal">
//                 <h1>Internal</h1>
//                 <img src={internal_down_icon} alt="" />
//               </div>
//               <div className="see_all_chats">
//                 <p>See All</p>
//               </div>
//             </div>
//             <div className="admin_chat">
//               <div className="admin-img">
//                 <img src={admin_img} alt="" />
//               </div>
//               <div className="admin_detail">
//                 <div className="admin_sms_timming">
//                   <h1>Admins</h1>
//                   <p>Just Now</p>
//                 </div>
//                 <div className="admin_sms">
//                   <p>Lorem ipsum dolor sit amet amiti.</p>
//                 </div>
//               </div>
//             </div>

//             <div className="admin_chat dr_chat">
//               <div className="admin-img dr_img">
//                 <img src={doctor_img} alt="" />
//               </div>
//               <div className="admin_detail dr_detail">
//                 <div className="admin_sms_timming dr_sms_timming">
//                   <h1>Doctors</h1>
//                   <p>10:36 AM</p>
//                 </div>
//                 <div className="admin_sms dr_sms">
//                   <p>Vivamus varius nunc neque..</p>
//                   <span>3</span>
//                 </div>
//               </div>
//             </div>

//             <div className="admin_chat">
//               <div className="admin-img">
//                 <img src={admin_img} alt="" />
//               </div>
//               <div className="admin_detail">
//                 <div className="admin_sms_timming">
//                   <h1>Nurse Freeha</h1>
//                   <p>Just Now</p>
//                 </div>
//                 <div className="admin_sms">
//                   <p>Lorem ipsum dolor sit amet amiti.</p>
//                 </div>
//               </div>
//             </div>

//             <div className="admin_chat dr_chat">
//               <div className="admin-img dr_img">
//                 <img src={doctor_img} alt="" />
//               </div>
//               <div className="admin_detail dr_detail">
//                 <div className="admin_sms_timming dr_sms_timming">
//                   <h1>Doctor Josh</h1>
//                   <p>10:36 AM</p>
//                 </div>
//                 <div className="admin_sms dr_sms">
//                   <p>Vivamus varius nunc neque..</p>
//                   <span>3</span>
//                 </div>
//               </div>
//             </div>
//           </div>
//           <div className="josh_chat">
//             <div className="josh_chat_container">
//               <div className="main_chat">
//                 <div className="j_c_head">
//                   <div className="j_c_active_status">
//                     <div className="j_c_img">
//                       <img src={chat_client_img} alt="" />
//                     </div>
//                     <div className="j_c_status">
//                       <h1>Client Name</h1>
//                       <p>Active</p>
//                     </div>
//                   </div>
//                   <div className="j_c_basic_detail">
//                     <div className="j_c_icons">
//                       <img
//                         className="j_c_detail"
//                         src={john_chat_detail_icon}
//                         alt=""
//                       />
//                       <img
//                         className="j_c_screen_zoom_out"
//                         src={john_chat_screen_zoom_out_icon}
//                         alt=""
//                       />
//                       <img
//                         className="j_c_screen_zoom_im"
//                         src={john_chat_screen_zoom_in_icon}
//                         alt=""
//                       />
//                       <Dropdown align="end">
//                         <Dropdown.Toggle className="j_c_menu">
//                           <img
//                             className="j_c_dialogs"
//                             src={john_chat_dialogs_icon}
//                             alt=""
//                           />
//                         </Dropdown.Toggle>
//                         <Dropdown.Menu className="j_c_dropdown_menu">
//                           <Dropdown.Item
//                             className="refer"
//                             onClick={handleShowReferDialog}
//                           >
//                             {" "}
//                             Refer{" "}
//                           </Dropdown.Item>
//                           <Dropdown.Item
//                             className="modify_access"
//                             onClick={handleShowChatAccessDialog}
//                           >
//                             {" "}
//                             Modify Access{" "}
//                           </Dropdown.Item>
//                         </Dropdown.Menu>
//                       </Dropdown>
//                     </div>
//                   </div>
//                 </div>
//                 <div className="vertically_chat_screen">
//                   <div className="twice_chat">
//                     <div className="receive_chat">
//                       <div className="receive_sms">
//                         <p>Morbi ullamcorper quis est et.</p>
//                         <div className="empty"></div>
//                       </div>
//                       <div className="staff_time">
//                         <div className="staff_name">
//                           <p>Staff Name</p>
//                         </div>
//                         <div className="time">
//                           <img src={chat_lock_icon} alt="" />
//                           <p>11:22</p>
//                         </div>
//                       </div>
//                     </div>

//                     <div className="send_chat">
//                       <div className="send_sms">
//                         <p>
//                           Integer quis eros quis et, vestibulum lobortis tortor,
//                           eleifend eleifend arcu.
//                         </p>
//                       </div>
//                       <div className="send_sms_time">
//                         <p>08:22</p>
//                       </div>
//                     </div>

//                     <div className="access_notification">
//                       <div className="access_sms">
//                         <p>Only you have access to this chat</p>
//                       </div>
//                       <div className="access_sms_timing">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>11:22</p>
//                       </div>
//                     </div>

//                     <div className="receive_chat">
//                       <div className="receive_sms">
//                         <p>Morbi ullamcorper quis est et.</p>
//                         <div className="empty"></div>
//                       </div>
//                       <div className="staff_time">
//                         <div className="staff_name">
//                           <p>Staff Name</p>
//                         </div>
//                         <div className="time">
//                           <img src={chat_lock_icon} alt="" />
//                           <p>11:22</p>
//                         </div>
//                       </div>
//                     </div>

//                     <div className="send_chat">
//                       <div className="imgs">
//                         <img src={send_img_1} alt="" />
//                         <img src={send_img_2} alt="" />
//                         <img src={send_img_3} alt="" />
//                       </div>
//                       <div className="send_sms_time">
//                         <div className="access_sms_timing">
//                           <img src={chat_lock_icon} alt="" />
//                           <p>08:22</p>
//                         </div>
//                       </div>
//                     </div>

//                     <div className="receive_chat">
//                       <div className="receive_sms">
//                         <p>Morbi ullamcorper quis est et.</p>
//                         <div className="empty"></div>
//                       </div>
//                       <div className="staff_time">
//                         <div className="staff_name">
//                           <p>Staff Name</p>
//                         </div>
//                         <div className="time">
//                           <img src={chat_lock_icon} alt="" />
//                           <p>11:22</p>
//                         </div>
//                       </div>
//                     </div>

//                     <div className="access_notification">
//                       <div className="access_sms">
//                         <p>
//                           Doctor Josh, Freeha and 7 Other have access to this
//                           chat
//                         </p>
//                       </div>
//                       <div className="access_sms_timing">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>11:22</p>
//                       </div>
//                     </div>
//                     <div className="chat_suggestions">
//                       <div className="suggestion_1">
//                         <p>Replies</p>
//                         <p>Message</p>
//                         <p>Nope</p>
//                         <p>Quick Replies</p>
//                       </div>
//                       <div className="suggestion_2">
//                         <p>Quick Replies</p>
//                         <p>Replies</p>
//                         <p>Message</p>
//                         <p>Nope</p>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//                 <div className="test_msg">
//                   <div className="attach_file">
//                     <img src={attach_file_icon} alt="" />
//                   </div>
//                   <div className="text_smg">
//                     <input
//                       type="text"
//                       name="sms"
//                       id="sms"
//                       className="input"
//                       placeholder="Type a Message"
//                     />
//                   </div>
//                   <div className="send_sms">
//                     <img src={send_sms_icon} alt="" />
//                   </div>
//                 </div>
//               </div>
//             </div>

//             {/* APPOINTMENT CHAT 1 */}

//             <div className="main_chat">
//               <div className="j_c_head">
//                 <div className="j_c_active_status">
//                   <div className="j_c_img">
//                     <img src={chat_client_img} alt="" />
//                   </div>
//                   <div className="j_c_status">
//                     <h1>Client Name</h1>
//                     <p>Active</p>
//                   </div>
//                 </div>
//                 <div className="j_c_basic_detail">
//                   <div className="j_c_icons">
//                     <img
//                       className="j_c_detail"
//                       src={john_chat_detail_icon}
//                       alt=""
//                     />
//                     <img
//                       className="j_c_screen_zoom_im"
//                       src={john_chat_screen_zoom_in_icon}
//                       alt=""
//                     />
//                     <Dropdown align="end">
//                       <Dropdown.Toggle className="j_c_menu">
//                         <img
//                           className="j_c_dialogs"
//                           src={john_chat_dialogs_icon}
//                           alt=""
//                         />
//                       </Dropdown.Toggle>
//                       <Dropdown.Menu className="j_c_dropdown_menu">
//                         <Dropdown.Item
//                           className="refer"
//                           onClick={handleShowReferDialog}
//                         >
//                           {" "}
//                           Refer{" "}
//                         </Dropdown.Item>
//                         <Dropdown.Item
//                           className="modify_access"
//                           onClick={handleShowChatAccessDialog}
//                         >
//                           Modify Access
//                         </Dropdown.Item>
//                       </Dropdown.Menu>
//                     </Dropdown>
//                   </div>
//                 </div>
//               </div>
//               <div className="vertically_chat_screen">
//                 <div className="twice_chat">
//                   <div className="appointment_notification">
//                     <p>I need a book Appointment</p>
//                     <img src={appointment_pin_icon} alt="" />
//                   </div>

//                   <div className="send_chat">
//                     <div className="send_sms">
//                       <p>
//                         Integer quis eros quis et, vestibulum lobortis tortor,
//                         eleifend eleifend arcu.
//                       </p>
//                     </div>
//                     <div className="send_sms_time">
//                       <p>08:22</p>
//                     </div>
//                   </div>

//                   <div className="receive_chat">
//                     <div className="receive_sms">
//                       <p>Morbi ullamcorper quis est et.</p>
//                       <div className="empty"></div>
//                     </div>
//                     <div className="staff_time">
//                       <div className="staff_name">
//                         <p>Staff Name</p>
//                       </div>
//                       <div className="time">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>11:22</p>
//                       </div>
//                     </div>
//                   </div>

//                   <div className="send_chat">
//                     <div className="imgs">
//                       <img src={send_img_1} alt="" />
//                       <img src={send_img_2} alt="" />
//                       <img src={send_img_3} alt="" />
//                     </div>
//                     <div className="send_sms_time">
//                       <div className="access_sms_timing">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>08:22</p>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>

//             {/* APPOINTMENT CHAT 2 */}

//             <div className="main_chat">
//               <div className="j_c_head">
//                 <div className="j_c_active_status">
//                   <div className="j_c_img">
//                     <img src={chat_client_img} alt="" />
//                   </div>
//                   <div className="j_c_status">
//                     <h1>Client Name</h1>
//                     <p>Active</p>
//                   </div>
//                 </div>
//                 <div className="j_c_basic_detail">
//                   <div className="j_c_icons">
//                     <img
//                       className="j_c_detail"
//                       src={john_chat_detail_icon}
//                       alt=""
//                     />
//                     <img
//                       className="j_c_screen_zoom_im"
//                       src={john_chat_screen_zoom_in_icon}
//                       alt=""
//                     />
//                     <Dropdown align="end">
//                       <Dropdown.Toggle className="j_c_menu">
//                         <img
//                           className="j_c_dialogs"
//                           src={john_chat_dialogs_icon}
//                           alt=""
//                         />
//                       </Dropdown.Toggle>
//                       <Dropdown.Menu className="j_c_dropdown_menu">
//                         <Dropdown.Item
//                           className="refer"
//                           onClick={handleShowReferDialog}
//                         >
//                           {" "}
//                           Refer{" "}
//                         </Dropdown.Item>
//                         <Dropdown.Item
//                           className="modify_access"
//                           onClick={handleShowChatAccessDialog}
//                         >
//                           Modify Access
//                         </Dropdown.Item>
//                       </Dropdown.Menu>
//                     </Dropdown>
//                   </div>
//                 </div>
//               </div>
//               <div className="vertically_chat_screen">
//                 <div className="twice_chat">
//                   <div className="appointment_notification">
//                     <p>I need a book Appointment</p>
//                     <img src={appointment_pin_icon} alt="" />
//                   </div>

//                   <div className="send_chat">
//                     <div className="send_sms">
//                       <p>
//                         Integer quis eros quis et, vestibulum lobortis tortor,
//                         eleifend eleifend arcu.
//                       </p>
//                     </div>
//                     <div className="send_sms_time">
//                       <p>08:22</p>
//                     </div>
//                   </div>

//                   <div className="receive_chat">
//                     <div className="receive_sms">
//                       <p>Morbi ullamcorper quis est et.</p>
//                       <div className="empty"></div>
//                     </div>
//                     <div className="staff_time">
//                       <div className="staff_name">
//                         <p>Staff Name</p>
//                       </div>
//                       <div className="time">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>11:22</p>
//                       </div>
//                     </div>
//                   </div>

//                   <div className="send_chat">
//                     <div className="imgs">
//                       <img src={send_img_1} alt="" />
//                       <img src={send_img_2} alt="" />
//                       <img src={send_img_3} alt="" />
//                     </div>
//                     <div className="send_sms_time">
//                       <div className="access_sms_timing">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>08:22</p>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>

//             {/* CHAT 3 */}

//             <div className="main_chat">
//               <div className="j_c_head">
//                 <div className="j_c_active_status">
//                   <div className="j_c_img">
//                     <img src={chat_client_img} alt="" />
//                   </div>
//                   <div className="j_c_status">
//                     <h1>Client Name</h1>
//                     <p>Active</p>
//                   </div>
//                 </div>
//                 <div className="j_c_basic_detail">
//                   <div className="j_c_icons">
//                     <img
//                       className="j_c_detail"
//                       src={john_chat_detail_icon}
//                       alt=""
//                     />
//                     <img
//                       className="j_c_screen_zoom_im"
//                       src={john_chat_screen_zoom_in_icon}
//                       alt=""
//                     />
//                     <Dropdown align="end">
//                       <Dropdown.Toggle className="j_c_menu">
//                         <img
//                           className="j_c_dialogs"
//                           src={john_chat_dialogs_icon}
//                           alt=""
//                         />
//                       </Dropdown.Toggle>
//                       <Dropdown.Menu className="j_c_dropdown_menu">
//                         <Dropdown.Item
//                           className="refer"
//                           onClick={handleShowReferDialog}
//                         >
//                           {" "}
//                           Refer{" "}
//                         </Dropdown.Item>
//                         <Dropdown.Item
//                           className="modify_access"
//                           onClick={handleShowChatAccessDialog}
//                         >
//                           {" "}
//                           Modify Access{" "}
//                         </Dropdown.Item>
//                       </Dropdown.Menu>
//                     </Dropdown>
//                   </div>
//                 </div>
//               </div>
//               <div className="vertically_chat_screen">
//                 <div className="twice_chat">
//                   <div className="receive_chat">
//                     <div className="receive_sms">
//                       <p>Morbi ullamcorper quis est et.</p>
//                       <div className="empty"></div>
//                     </div>
//                     <div className="staff_time">
//                       <div className="staff_name">
//                         <p>Staff Name</p>
//                       </div>
//                       <div className="time">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>11:22</p>
//                       </div>
//                     </div>
//                   </div>

//                   <div className="send_chat">
//                     <div className="send_sms">
//                       <p>
//                         Integer quis eros quis et, vestibulum lobortis tortor,
//                         eleifend eleifend arcu.
//                       </p>
//                     </div>
//                     <div className="send_sms_time">
//                       <p>08:22</p>
//                     </div>
//                   </div>

//                   <div className="receive_chat">
//                     <div className="receive_sms">
//                       <p>Morbi ullamcorper quis est et.</p>
//                       <div className="empty"></div>
//                     </div>
//                     <div className="staff_time">
//                       <div className="staff_name">
//                         <p>Staff Name</p>
//                       </div>
//                       <div className="time">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>11:22</p>
//                       </div>
//                     </div>
//                   </div>

//                   <div className="send_chat">
//                     <div className="imgs">
//                       <img src={send_img_1} alt="" />
//                       <img src={send_img_2} alt="" />
//                       <img src={send_img_3} alt="" />
//                     </div>
//                     <div className="send_sms_time">
//                       <div className="access_sms_timing">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>08:22</p>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>

//             {/* CHAT 4 */}

//             <div className="main_chat">
//               <div className="j_c_head">
//                 <div className="j_c_active_status">
//                   <div className="j_c_img">
//                     <img src={chat_client_img} alt="" />
//                   </div>
//                   <div className="j_c_status">
//                     <h1>Client Name</h1>
//                     <p>Active</p>
//                   </div>
//                 </div>
//                 <div className="j_c_basic_detail">
//                   <div className="j_c_icons">
//                     <img
//                       className="j_c_detail"
//                       src={john_chat_detail_icon}
//                       alt=""
//                     />
//                     <img
//                       className="j_c_screen_zoom_im"
//                       src={john_chat_screen_zoom_in_icon}
//                       alt=""
//                     />
//                     <Dropdown align="end">
//                       <Dropdown.Toggle className="j_c_menu">
//                         <img
//                           className="j_c_dialogs"
//                           src={john_chat_dialogs_icon}
//                           alt=""
//                         />
//                       </Dropdown.Toggle>
//                       <Dropdown.Menu className="j_c_dropdown_menu">
//                         <Dropdown.Item
//                           className="refer"
//                           onClick={handleShowReferDialog}
//                         >
//                           {" "}
//                           Refer{" "}
//                         </Dropdown.Item>
//                         <Dropdown.Item
//                           className="modify_access"
//                           onClick={handleShowChatAccessDialog}
//                         >
//                           {" "}
//                           Modify Access{" "}
//                         </Dropdown.Item>
//                       </Dropdown.Menu>
//                     </Dropdown>
//                   </div>
//                 </div>
//               </div>
//               <div className="vertically_chat_screen">
//                 <div className="twice_chat">
//                   <div className="receive_chat">
//                     <div className="receive_sms">
//                       <p>Morbi ullamcorper quis est et.</p>
//                       <div className="empty"></div>
//                     </div>
//                     <div className="staff_time">
//                       <div className="staff_name">
//                         <p>Staff Name</p>
//                       </div>
//                       <div className="time">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>11:22</p>
//                       </div>
//                     </div>
//                   </div>

//                   <div className="send_chat">
//                     <div className="send_sms">
//                       <p>
//                         Integer quis eros quis et, vestibulum lobortis tortor,
//                         eleifend eleifend arcu.
//                       </p>
//                     </div>
//                     <div className="send_sms_time">
//                       <p>08:22</p>
//                     </div>
//                   </div>

//                   <div className="receive_chat">
//                     <div className="receive_sms">
//                       <p>Morbi ullamcorper quis est et.</p>
//                       <div className="empty"></div>
//                     </div>
//                     <div className="staff_time">
//                       <div className="staff_name">
//                         <p>Staff Name</p>
//                       </div>
//                       <div className="time">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>11:22</p>
//                       </div>
//                     </div>
//                   </div>

//                   <div className="send_chat">
//                     <div className="imgs">
//                       <img src={send_img_1} alt="" />
//                       <img src={send_img_2} alt="" />
//                       <img src={send_img_3} alt="" />
//                     </div>
//                     <div className="send_sms_time">
//                       <div className="access_sms_timing">
//                         <img src={chat_lock_icon} alt="" />
//                         <p>08:22</p>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>

//       <ReferDialog show={referDialog} onClose={handleCloseReferDialog} />
//       <ChatAccessDialog
//         show={chatAccessDialog}
//         onClose={handleCloseChatAccessDialog}
//       />
//     </>
//   );
// };

import React from 'react'

const JoshChat = () => {
  return (
    <div>
      
    </div>
  )
}