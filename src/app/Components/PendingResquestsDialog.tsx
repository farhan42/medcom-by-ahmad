import React from "react";
import { Modal } from "react-bootstrap";

import P_R_Dialog_Cross from "../../assets/icons/cross.svg";

const courtney_head_img = require("../../assets/Images/courtney.png");

interface Props {
  show: boolean;
  onClose: () => void;
}

export const PendingResquestsDialog = (props: Props): JSX.Element => {
  return (
    <Modal
      className="pending_resquests_dialog_modal"
      size="lg"
      show={props.show}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <div className="p_r_container">
        <div className="p_r_head">
          <img src={courtney_head_img} alt="" />
          <h1>Kourtney Cleveland</h1>
          <img
            className="p_r_cross"
            onClick={props.onClose}
            src={P_R_Dialog_Cross}
            alt=""
          />
        </div>
        <div className="p_r_input_fields">
          <div className="p_r_flex_input_fields">
            <div className="email_input inputs">
              <label htmlFor="email"> Email </label>
              <input
                type="email"
                name="email"
                id="email"
                className="input"
                placeholder="trungkienspktnd@gamail.com"
              />
            </div>
            <div className="phone_number_input inputs">
              <label htmlFor="phonenumber"> Phone Number </label>
              <input
                type="phonenumber"
                name="phonenumber"
                id="phonenumber"
                className="input"
                placeholder="(505) 555-0125"
              />
            </div>
          </div>

          <div className="address_input inputs">
            <label htmlFor="address"> Address </label>
            <input
              type="text"
              name="address"
              id="address"
              placeholder="7529 E. Pecan St."
              className="input"
            />
          </div>

          <div className="p_r_flex_input_fields">
            <div className="patient_id_input inputs">
              <label htmlFor="patient_id"> Patient ID </label>
              <input
                type="patient_id"
                name="patient_id"
                id="patient_id"
                className="input"
                placeholder="323686"
              />
            </div>
            <div className="insurance_input inputs">
              <label htmlFor="insurance"> Insurance ID </label>
              <input
                type="insurance"
                name="insurance"
                id="insurance"
                className="input"
                placeholder="3236254"
              />
            </div>
          </div>

          <div className="remarks_input inputs">
            <label htmlFor="remarks"> Remarks </label>
            <input
              type="remarks"
              name="remarks"
              id="remarks"
              placeholder="Remarks"
              className="input"
            />
          </div>
        </div>

        <div className="p_r_buttons">
          <button className="p_r_approve" onClick={props.onClose}>
            Approve
          </button>
          <button className="p_r_cancel" onClick={props.onClose}>
            Cancel Request
          </button>
          <button className="p_r_block" onClick={props.onClose}>
            Block
          </button>
        </div>
      </div>
    </Modal>
  );
};
