import React from "react";
import { Modal } from "react-bootstrap";

import cross_icon from "../../assets/icons/cross.svg";
import refer_search_icon from "../../assets/icons/search.svg";

const patient_img_1 = require("../../assets/Images/patient_img_1.png");

interface Props {
  show: boolean;
  onClose: () => void;
}

export const ReferDialog = (props: Props): JSX.Element => {
  return (
    <>
      <Modal
        className="refer_dialog modal"
        show={props.show}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <div className="refer_container">
          <div className="reifer_head">
            <h1>Refer</h1>
            <img onClick={props.onClose} src={cross_icon} alt="" />
          </div>
          <div className="refer_input">
            <input
              type="text"
              name="search"
              id="search"
              className="input"
              placeholder="Search..."
            />
            <img src={refer_search_icon} alt="" />
          </div>
          <div className="refer_detail">
            <table>
              <tr>
                <th>
                  <input className="input" type="checkbox" />
                </th>
                <th>Staff Name</th>
                <th>Role</th>
              </tr>

              <tr>
                <td>
                  <input type="checkbox" />
                </td>
                <td className="gap">
                  <img src={patient_img_1} alt="" />
                  <tr>Wazuka Inoguchi</tr>
                </td>
                <td> Analytics Admin </td>
              </tr>

              <tr>
                <td>
                  <input type="checkbox" />
                </td>
                <td className="gap">
                  <img src={patient_img_1} alt="" />
                  <tr>Wazuka Inoguchi</tr>
                </td>
                <td> Reporter </td>
              </tr>

              <tr>
                <td>
                  <input type="checkbox" />
                </td>
                <td className="gap">
                  <img src={patient_img_1} alt="" />
                  <tr>Amalie Huerta</tr>
                </td>
                <td> Marketer </td>
              </tr>

              <tr>
                <td>
                  <input type="checkbox" />
                </td>
                <td className="gap">
                  <img src={patient_img_1} alt="" />
                  <tr>Tatiana Kozlov</tr>
                </td>
                <td> Accountant </td>
              </tr>

              <tr>
                <td>
                  <input type="checkbox" />
                </td>
                <td className="gap">
                  <img src={patient_img_1} alt="" />
                  <tr>Eli Dotson</tr>
                </td>
                <td> Sales Manager </td>
              </tr>

              <tr>
                <td>
                  <input type="checkbox" />
                </td>
                <td className="gap">
                  <img src={patient_img_1} alt="" />
                  <tr>Dilara Hinton</tr>
                </td>
                <td> Team Editor </td>
              </tr>

              <tr>
                <td>
                  <input type="checkbox" />
                </td>
                <td className="gap">
                  <img src={patient_img_1} alt="" />
                  <tr>Juniper Wolf</tr>
                </td>
                <td> Bot Analyst </td>
              </tr>
            </table>
            <div className="refer_button">
              <button
                type="button"
                className="refer_btn"
                onClick={props.onClose}
              >
                {" "}
                Refer{" "}
              </button>
            </div>
          </div>
        </div>
      </Modal>
    </>
  );
};
