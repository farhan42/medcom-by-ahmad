import React from "react";
import { NavLink, useNavigate } from "react-router-dom";

import sidebar_home_icon from "../../assets/icons/Sidebar_Home_icon.svg";
import sidebar_staff_icon from "../../assets/icons/Sidebar_Staff_icon.svg";
import sidebar_patients_icon from "../../assets/icons/Sidebar_Patient_icon.svg";
import sidebar_chat_icon from "../../assets/icons/Sidebar_Chat_icon.svg";
import sidebar_notification_icon from "../../assets/icons/Sidebar_notification.svg";
import sidebar_setting_icon from "../../assets/icons/Sidebar_Setting_icon.svg";
import sidebar_logout_icon from "../../assets/icons/Group.svg";
import { useAuthContext } from "../contexts";

const sidebar_log = require("../../assets/Images/Home_page_img.png");

export const Sidebar = (): JSX.Element => {
  const context = useAuthContext();
  const navigate = useNavigate();

  const hanldeLogout = () => {
    context.updateUser(null);
    navigate("/login");
  };

  return (
    <div className="my_sidebar">
      <div className="sidebar_container">
        <div className="sidebar_links">
          <div className="sidebar_logo">
            <img src={sidebar_log} alt="" />
          </div>
          <div className="side_links">
            <ul>
              <li>
                <NavLink className="hom" to="/home">
                  {" "}
                  <img src={sidebar_home_icon} alt="" /> Home{" "}
                </NavLink>
              </li>
              <li>
                <NavLink className="staf" to="/staff">
                  {" "}
                  <img src={sidebar_patients_icon} alt="" /> Staff{" "}
                </NavLink>
              </li>
              <li>
                <NavLink className="pat" to="/patients">
                  {" "}
                  <img src={sidebar_staff_icon} alt="" /> Patients{" "}
                </NavLink>
              </li>
              <li>
                <NavLink className="chat" to="/chat">
                  {" "}
                  <img src={sidebar_chat_icon} alt="" /> chat{" "}
                </NavLink>
              </li>
              <li>
                <NavLink className="not" to="/notification">
                  {" "}
                  <img
                    src={sidebar_notification_icon}
                    alt=""
                  /> Notification <div className="red"></div>{" "}
                </NavLink>
              </li>
              <li>
                <NavLink className="set" to="/settings">
                  {" "}
                  <img src={sidebar_setting_icon} alt="" /> Settings{" "}
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="logout_icon">
            <div className="lout" onClick={hanldeLogout}> <img src={sidebar_logout_icon} alt="" /> Logout{" "}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
