import React from "react";
import { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import {
  CheckEmailDialog,
  ForgotPasswordDialog,
  FormikErrorMessage,
} from "../../Components";

import authLogo from "../../../assets/icons/auth-logo.svg";
import { useFormik } from "formik";
import { LoginForm } from "../../../Validators";
import { FormikValidator } from "../../../shared/utility";
import { API } from "../../../api";
import { UserRealmEnum } from "../../../interfaces";
import { useAuthContext } from "../../contexts";
const logo = require("../../../assets/Images/Med_com_logo.png");
const registrationImg = require("../../../assets/Images/landing_page_front_img.png");

export const Login = (): JSX.Element => {
  const [forgotPasswordDialog, setForgotPasswortDialog] =
    useState<boolean>(false);
  const [checkEmailDialog, setCheckEmailDialog] = useState<boolean>(false);

  const handleShowForgotPasswordDialog = () => {
    setForgotPasswortDialog(true);
  };

  const handleCloseForgotPasswordDialog = () => {
    setForgotPasswortDialog(false);
    setCheckEmailDialog(true);
  };

  const handleClsoeForgotPasswordDialogCrossIcon = () => {
    setForgotPasswortDialog(false);
  };

  const handleCloseCheckEmailDialog = () => {
    setCheckEmailDialog(false);
  };

  const context = useAuthContext();
  const navigate = useNavigate();

  const loginForm = useFormik<LoginForm>({
    initialValues: {
      identifier: "",
      password: "",
      realm: UserRealmEnum.Admin,
    },

    onSubmit: (values) => {
      API.Auth.Login(values)
        .then((response) => {
          context.updateUser(response.data);
          navigate("/home");
          console.log("Success!", response);
        })
        .catch((error) => {
          console.error("failed", error);
        });
    },
    validate: FormikValidator.validator(LoginForm),
  });

  return (
    <>
      <div className="medcom">
        <div className="auth-bg">
          <div className="logo">
            <img src={authLogo} />
          </div>
        </div>
        <div className="login_registration">
          <div className="login_register_head">
            <h1> Welcome Back to your Clinic </h1>
          </div>
          <form
            onSubmit={loginForm.handleSubmit}
            className="login_register_form"
          >
            <div className="input_field">
              <label htmlFor="identifier"> Email Address </label>
              <input
                name="identifier"
                id="identifier"
                type="email"
                className="input"
                placeholder="Identifier"
                onBlur={loginForm.handleBlur}
                onChange={loginForm.handleChange}
              />
              <FormikErrorMessage
                formik={loginForm}
                name="identifier"
                render={(error) => <span className="error">{error}</span>}
              />
              <label htmlFor="password"> Password </label>
              <input
                name="password"
                id="password"
                type="Password"
                className="input"
                placeholder="Password"
                onBlur={loginForm.handleBlur}
                onChange={loginForm.handleChange}
              />
              <FormikErrorMessage
                formik={loginForm}
                name="password"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="login_btn">
              <button type="submit" className="li_btn">
                Login
              </button>
            </div>
            <div className="forgot_password">
              <button type="button" onClick={handleShowForgotPasswordDialog}>
                {" "}
                Forgot password?{" "}
              </button>
            </div>
          </form>
          <div className="create_account">
            <p>
              Don't have an account
              <NavLink to="/register"> Create Account </NavLink>
            </p>
          </div>
        </div>
      </div>
      <ForgotPasswordDialog
        show={forgotPasswordDialog}
        onClose={handleCloseForgotPasswordDialog}
        onHide={handleClsoeForgotPasswordDialogCrossIcon}
      />
      <CheckEmailDialog
        show={checkEmailDialog}
        onclose={handleCloseCheckEmailDialog}
      />
    </>
  );
};
