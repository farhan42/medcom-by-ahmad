import { useFormik } from "formik";
import React from "react";
import { NavLink } from "react-router-dom";
import { API } from "../../../api";
import authLogo from "../../../assets/icons/auth-logo.svg";
import { FormikValidator } from "../../../shared/utility";
import { RegisterForm } from "../../../Validators";
import { FormikErrorMessage } from "../../Components";
// const logo = require("../../../assets/Images/Med_com_logo.png");
// const registrationImg = require("../../../assets/Images/landing_page_front_img.png");

export const Register = (): JSX.Element => {
  const registerForm = useFormik<RegisterForm>({
    initialValues: {
      title: "",
      email: "",
      phoneNumber: "",
      password: "",
      confirmPassword: "",
      address: "",
    },
    onSubmit: (values) => {
      API.Auth.Register(values)
        .then((response) => {
          console.log("Success!");
        })
        .catch((error) => {
          console.error("failed");
        });
    },
    validate: FormikValidator.validator(RegisterForm),
  });

  return (
    <div className="medcom">
      <div className="auth-bg">
        <div className="logo">
          <img src={authLogo} />
        </div>
      </div>
      <div className="registration">
        <div className="registration_head">
          <h1> Register your Clinic </h1>
        </div>
        <form
          onSubmit={registerForm.handleSubmit}
          className="registration_form"
        >
          <div className="input_field">
            <label htmlFor="title"> Clinic Name </label>
            <input
              name="title"
              id="title"
              type="text"
              className="input"
              placeholder="Clinic Name"
              onBlur={registerForm.handleBlur}
              onChange={registerForm.handleChange}
            />
            <FormikErrorMessage
              formik={registerForm}
              name="title"
              render={(error) => <span className="error">{error}</span>}
            />
            <label htmlFor="email"> Email Address </label>
            <input
              name="email"
              id="email"
              type="email"
              className="input"
              placeholder="Email Address"
              onBlur={registerForm.handleBlur}
              onChange={registerForm.handleChange}
            />
            <FormikErrorMessage
              formik={registerForm}
              name="email"
              render={(error) => <span className="error">{error}</span>}
            />
            <label htmlFor="phoneNumber">Phone Number </label>
            <input
              name="phoneNumber"
              id="phoneNumber"
              type="text"
              className="input"
              placeholder="phone Number"
              onBlur={registerForm.handleBlur}
              onChange={registerForm.handleChange}
            />
            <FormikErrorMessage
              formik={registerForm}
              name="phoneNumber"
              render={(error) => <span className="error">{error}</span>}
            />
            <label htmlFor="password"> Password </label>
            <input
              name="password"
              id="password"
              type="Password"
              className="input"
              placeholder="Password"
              onBlur={registerForm.handleBlur}
              onChange={registerForm.handleChange}
            />
            <FormikErrorMessage
              formik={registerForm}
              name="password"
              render={(error) => <span className="error">{error}</span>}
            />
            <label htmlFor="confirmPassword"> Confirm Password </label>
            <input
              name="confirmPassword"
              id="confirmPassword"
              type="password"
              className="input"
              placeholder="Confirm Password"
              onBlur={registerForm.handleBlur}
              onChange={registerForm.handleChange}
            />
            <FormikErrorMessage
              formik={registerForm}
              name="confirmPassword"
              render={(error) => <span className="error">{error}</span>}
            />
            <label htmlFor="address"> Address </label>
            <input
              name="address"
              id="address"
              type="text"
              className="input"
              placeholder="Address"
              onBlur={registerForm.handleBlur}
              onChange={registerForm.handleChange}
            />
            <FormikErrorMessage
              formik={registerForm}
              name="address"
              render={(error) => <span className="error">{error}</span>}
            />
          </div>
          <div className="form_btn">
            <button type="submit" className="f_btn">
              {" "}
              Register Clinic{" "}
            </button>
          </div>
        </form>
        <div className="register_btn">
          <p>
            {" "}
            Already have an account <NavLink to="/login"> Login </NavLink>{" "}
          </p>
        </div>
      </div>
    </div>
  );
};
