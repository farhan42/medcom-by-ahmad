import React from "react";
import {
  AppointmentChat,
  BoxChat,
  ChatLogs,
  LongChat,
  Media,
} from "./components";

export const MainChat = (): JSX.Element => {
  return (
    <div className="chat_main_layout">
      <div className="c_logs">
        <ChatLogs />
      </div>
      <div className="log_chats">
        <div className="long_chat">
          <LongChat />
        </div>
        <div className="chat_boxes">
          <AppointmentChat />
          <AppointmentChat />
          <BoxChat />
          <BoxChat />
        </div>
        <div>
          <Media />
        </div>
      </div>
    </div>
  );
};
