import React from "react";
import { NavLink } from "react-router-dom";
import internal_down_icon from "../../../../../assets/icons/internal_down.svg";

const admin_img = require("../../../../../assets/Images/admin.png");
const doctor_img = require("../../../../../assets/Images/doctor.png");

export const ChatLogs = (): JSX.Element => {
  return (
    <div className="medcom_chat">
      <div className="chat_container">
        <div className="chat_list">
          <div className="chat_list_head">
            <h1>Chat</h1>
          </div>
          <div className="internal_chat_list">
            <div className="internal">
              <h1>Internal</h1>
              <img src={internal_down_icon} alt="" />
            </div>
            <div className="see_all_chats">
              <p>See All</p>
            </div>
          </div>
          <div className="logs">
            <div className="admin_chat">
              <div className="admin-img">
                <img src={admin_img} alt="" />
              </div>
              <div className="admin_detail">
                <div className="admin_sms_timming">
                  <h1>Admins</h1>
                  <p>Just Now</p>
                </div>
                <div className="admin_sms">
                  <p>Lorem ipsum dolor sit amet amiti.</p>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctors</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat">
              <div className="admin-img">
                <img src={admin_img} alt="" />
              </div>
              <div className="admin_detail">
                <div className="admin_sms_timming">
                  <h1>Nurse Freeha</h1>
                  <p>Just Now</p>
                </div>
                <div className="admin_sms">
                  <p>Lorem ipsum dolor sit amet amiti.</p>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>

            <div className="admin_chat dr_chat">
              <div className="admin-img dr_img">
                <img src={doctor_img} alt="" />
              </div>
              <div className="admin_detail dr_detail">
                <div className="admin_sms_timming dr_sms_timming">
                  <h1>Doctor Josh</h1>
                  <p>10:36 AM</p>
                </div>
                <div className="admin_sms dr_sms">
                  <p>Vivamus varius nunc neque..</p>
                  <span>3</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
