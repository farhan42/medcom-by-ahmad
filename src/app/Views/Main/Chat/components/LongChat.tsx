import React, { useState } from "react";

import internal_down_icon from "../../../../../assets/icons/internal_down.svg";
import john_chat_detail_icon from "../../../../../assets/icons/chat_client_detail.svg";
import john_chat_screen_zoom_out_icon from "../../../../../assets/icons/chat_client_screen_zoom_out.svg";
import john_chat_screen_zoom_in_icon from "../../../../../assets/icons/chat_client_screen_zoom_in.svg";
import john_chat_dialogs_icon from "../../../../../assets/icons/chat_client_dialogs_icon.svg";
import chat_lock_icon from "../../../../../assets/icons/chat_lock.svg";
import attach_file_icon from "../../../../../assets/icons/attach_file.svg";
import send_sms_icon from "../../../../../assets/icons/send_sms.svg";
import appointment_pin_icon from "../../assets/icons/pin_appointment.svg";
import { Dropdown } from "react-bootstrap";
import { ChatAccessDialog, ReferDialog } from "../../../../Components";

const admin_img = require("../../../../../assets/Images/admin.png");
const doctor_img = require("../../../../../assets/Images/doctor.png");
const chat_client_img = require("../../../../../assets/Images/chat_client.png");
const send_img_1 = require("../../../../../assets/Images/send_img.png");
const send_img_2 = require("../../../../../assets/Images/send_img_2.png");
const send_img_3 = require("../../../../../assets/Images/send_img_3.png");

export const LongChat = (): JSX.Element => {
  const [referDialog, setReferDialog] = useState<boolean>(false);
  const [chatAccessDialog, setChatAccessDialog] = useState<boolean>(false);

  const handleShowReferDialog = () => {
    setReferDialog(true);
  };

  const handleCloseReferDialog = () => {
    setReferDialog(false);
  };

  const handleShowChatAccessDialog = () => {
    setChatAccessDialog(true);
  };

  const handleCloseChatAccessDialog = () => {
    setChatAccessDialog(false);
  };
  return (
    <>
      <div className="josh_chat">
        <div className="josh_chat_container">
          <div className="main_chat">
            <div className="j_c_head">
              <div className="j_c_active_status">
                <div className="j_c_img">
                  <img src={chat_client_img} alt="" />
                </div>
                <div className="j_c_status">
                  <h1>Client Name</h1>
                  <p>Active</p>
                </div>
              </div>
              <div className="j_c_basic_detail">
                <div className="j_c_icons">
                  <img
                    className="j_c_detail"
                    src={john_chat_detail_icon}
                    alt=""
                  />
                  <img
                    className="j_c_screen_zoom_out"
                    src={john_chat_screen_zoom_out_icon}
                    alt=""
                  />
                  <img
                    className="j_c_screen_zoom_im"
                    src={john_chat_screen_zoom_in_icon}
                    alt=""
                  />
                  <Dropdown align="end">
                    <Dropdown.Toggle className="j_c_menu">
                      <img
                        className="j_c_dialogs"
                        src={john_chat_dialogs_icon}
                        alt=""
                      />
                    </Dropdown.Toggle>
                    <Dropdown.Menu className="j_c_dropdown_menu">
                      <Dropdown.Item
                        className="refer"
                        onClick={handleShowReferDialog}
                      >
                        {" "}
                        Refer{" "}
                      </Dropdown.Item>
                      <Dropdown.Item
                        className="modify_access"
                        onClick={handleShowChatAccessDialog}
                      >
                        {" "}
                        Modify Access{" "}
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
            </div>
            <div className="vertically_chat_screen">
              <div className="twice_chat">
                <div className="receive_chat">
                  <div className="receive_sms">
                    <p>Morbi ullamcorper quis est et.</p>
                    <div className="empty"></div>
                  </div>
                  <div className="staff_time">
                    <div className="staff_name">
                      <p>Staff Name</p>
                    </div>
                    <div className="time">
                      <img src={chat_lock_icon} alt="" />
                      <p>11:22</p>
                    </div>
                  </div>
                </div>

                <div className="send_chat">
                  <div className="send_sms">
                    <p>
                      Integer quis eros quis et, vestibulum lobortis tortor,
                      eleifend eleifend arcu.
                    </p>
                  </div>
                  <div className="send_sms_time">
                    <p>08:22</p>
                  </div>
                </div>

                <div className="access_notification">
                  <div className="access_sms">
                    <p>Only you have access to this chat</p>
                  </div>
                  <div className="access_sms_timing">
                    <img src={chat_lock_icon} alt="" />
                    <p>11:22</p>
                  </div>
                </div>

                <div className="receive_chat">
                  <div className="receive_sms">
                    <p>Morbi ullamcorper quis est et.</p>
                    <div className="empty"></div>
                  </div>
                  <div className="staff_time">
                    <div className="staff_name">
                      <p>Staff Name</p>
                    </div>
                    <div className="time">
                      <img src={chat_lock_icon} alt="" />
                      <p>11:22</p>
                    </div>
                  </div>
                </div>

                <div className="send_chat">
                  <div className="imgs">
                    <img src={send_img_1} alt="" />
                    <img src={send_img_2} alt="" />
                    <img src={send_img_3} alt="" />
                  </div>
                  <div className="send_sms_time">
                    <div className="access_sms_timing">
                      <img src={chat_lock_icon} alt="" />
                      <p>08:22</p>
                    </div>
                  </div>
                </div>

                <div className="receive_chat">
                  <div className="receive_sms">
                    <p>Morbi ullamcorper quis est et.</p>
                    <div className="empty"></div>
                  </div>
                  <div className="staff_time">
                    <div className="staff_name">
                      <p>Staff Name</p>
                    </div>
                    <div className="time">
                      <img src={chat_lock_icon} alt="" />
                      <p>11:22</p>
                    </div>
                  </div>
                </div>

                <div className="access_notification">
                  <div className="access_sms">
                    <p>
                      Doctor Josh, Freeha and 7 Other have access to this chat
                    </p>
                  </div>
                  <div className="access_sms_timing">
                    <img src={chat_lock_icon} alt="" />
                    <p>11:22</p>
                  </div>
                </div>
                <div className="chat_suggestions">
                  <div className="suggestion_1">
                    <p>Replies</p>
                    <p>Message</p>
                    <p>Nope</p>
                    <p>Quick Replies</p>
                  </div>
                  <div className="suggestion_2">
                    <p>Quick Replies</p>
                    <p>Replies</p>
                    <p>Message</p>
                    <p>Nope</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="test_msg">
              <div className="attach_file">
                <img src={attach_file_icon} alt="" />
              </div>
              <div className="text_smg">
                <input
                  type="text"
                  name="sms"
                  id="sms"
                  className="input"
                  placeholder="Type a Message"
                />
              </div>
              <div className="send_sms">
                <img src={send_sms_icon} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <ReferDialog show={referDialog} onClose={handleCloseReferDialog} />
      <ChatAccessDialog
        show={chatAccessDialog}
        onClose={handleCloseChatAccessDialog}
      />
    </>
  );
};
