import React from "react";

import attachment_files from "../../../../../assets/icons/attach_file.svg";
const chat_media_img = require("../../../../../assets/Images/media_takla.png");
const img_1 = require("../../../../../assets/Images/1.png");
const img_2 = require("../../../../../assets/Images/2.png");
const img_3 = require("../../../../../assets/Images/3.png");
const img_4 = require("../../../../../assets/Images/4.png");
const img_5 = require("../../../../../assets/Images/5.png");
const img_6 = require("../../../../../assets/Images/6.png");
const img_7 = require("../../../../../assets/Images/7.png");
const img_8 = require("../../../../../assets/Images/8.png");
const img_9 = require("../../../../../assets/Images/9.png");

export const Media = (): JSX.Element => {
  return (
    <div className="chat_media">
      <div className="media_container">
        <div className="media_head">
          <img src={chat_media_img} alt="" />
          <h1>Clinic Name</h1>
          <p></p>
        </div>
        <div className="attachments">
          <div className="attachments_head">
            <h1>Attachments</h1>
          </div>
          <div className="some_files">
            <p className="source_files"> Source file </p>
            <p className="all_files"> View All </p>
          </div>
          <div className="pdf_files">
            <div className="files">
              <div className="file">
                <img src={attachment_files} alt="" />
                <p>Final file.pdf</p>
              </div>
              <div className="file">
                <img src={attachment_files} alt="" />
                <p>Final file.pdf</p>
              </div>
              <div className="file">
                <img src={attachment_files} alt="" />
                <p>Final file.pdf</p>
              </div>
            </div>
          </div>
        </div>
        <div className="gallery">
          <div className="gallery_head">
            <h1>Picture & Videos</h1>
            <p>View all</p>
          </div>
          <div className="gallery_imgs">
            <img src={img_1} alt="" />
            <img src={img_2} alt="" />
            <img src={img_3} alt="" />
            <img src={img_4} alt="" />
            <img src={img_5} alt="" />
            <img src={img_6} alt="" />
            <img src={img_7} alt="" />
            <img src={img_8} alt="" />
            <img src={img_9} alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};
