import React from "react";

import approved_tick from "../../../assets/icons/approved_correct.svg";
import un_approved_x from "../../../assets/icons/un_approved_x.svg";
import patient_menu from "../../../assets/icons/patient_menu.svg";
import { Dropdown } from "react-bootstrap";

const approved = require("../../../assets/Images/approved.png");
const un_approved = require("../../../assets/Images/un_approved.png");
const total_patient = require("../../../assets/Images/total_patient.png");
const patient_img_1 = require("../../../assets/Images/patient_img_1.png");

export const Home = (): JSX.Element => {
  return (
    <div className="my_home">
      <div className="home_container">
        <div className="home_cards">
          <div className="approved_card">
            <div className="approved_img">
              <img src={approved} alt="" />
            </div>
            <div className="approved_detail">
              <div className="approved_tick">
                <p>Approved</p>
                <img src={approved_tick} alt="" />
              </div>
              <div className="approved_patients">
                <h1>0638</h1>
              </div>
            </div>
          </div>

          <div className="un_approved_card">
            <div className="un_approved_img">
              <img src={un_approved} alt="" />
            </div>
            <div className="un_approved_detail">
              <div className="un_approved_x">
                <p>Un Approved</p>
                <img src={un_approved_x} alt="" />
              </div>
              <div className="un_approved_patients">
                <h1>0043</h1>
              </div>
            </div>
          </div>

          <div className="patient_card">
            <div className="patient_card_img">
              <img src={total_patient} alt="" />
            </div>
            <div className="patients_detail">
              <p>Total Patient</p>
              <h1>0043</h1>
            </div>
          </div>
        </div>

        <div className="un_approved_patients_list">
          <div className="un_approved_patients_list_container">
            <div className="un_approved_patient_list_head">
              <h1>Un Approved Patients</h1>
              <img src={un_approved_x} alt="" />
            </div>
            <div className="patients_list">
              <table>
                <tr>
                  <th>Patient Name</th>
                  <th>Email</th>
                  <th>Patient ID</th>
                  <th>Insurance</th>
                  <th>PHone Number</th>
                  <th>Address</th>
                  <th></th>
                </tr>
                <tr>
                  <td className="gap"><img src={patient_img_1} alt="" /> Tatiana Kozlov</td>
                  <td> danghoang87hl@gmail.com </td>
                  <td> 283785 </td>
                  <td> 283785 </td>
                  <td> (201) 555-0124 </td>
                  <td> 1 Byworth Close, Brighton, </td>
                  {/* <td> <img src={patient_menu} alt="" /> </td> */}
                  <td>
                    <div className="h_menu">
                      <Dropdown align="end">
                        <Dropdown.Toggle className="home_menu"> <img src={patient_menu} alt="" /> </Dropdown.Toggle>
                        <Dropdown.Menu className="h_dropdown_menu">
                          <Dropdown.Item className="home_send_sms"> Send Message </Dropdown.Item>
                          <Dropdown.Item className="home_view_prof">
                            {" "}
                            View Profile{" "}
                          </Dropdown.Item>
                          <Dropdown.Item className="home_block">
                            {" "}
                            Block{" "}
                          </Dropdown.Item>
                          <div className="home_top_face"></div>
                        </Dropdown.Menu>
                      </Dropdown>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
