import React, { useEffect } from "react";
import { useDispatch } from "../../../store";
import { NotificationListThunk } from "../../../thunks";

const notification_img = require("../../../assets/Images/butler.png");
const wazuka_sms_img = require("../../../assets/Images/wazuka.png");
const tatiana_sms_img = require("../../../assets/Images/tatiana.png");
const doston_sms_img = require("../../../assets/Images/doston.png");
const wolf_sms_img = require("../../../assets/Images/wolf.png");

export const Notification = (): JSX.Element => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(NotificationListThunk());
  }, []);

  return (
    <div className="patients_notifications">
      <div className="notifications_container">
        <div className="notifications_head">
          <h1>Notifactions</h1>
        </div>
        <div className="user_notification">
          <div className="notification">
            <div className="notification_detail">
              <div className="notification_img">
                <img src={notification_img} alt="" />
              </div>
              <div className="notification_name">
                <h1>Anne Butler</h1>
                <p>
                  Approved By: <span> Doctor Hammad Gujjar </span>
                </p>
                <h6>Today</h6>
              </div>
            </div>
          </div>
          <div className="approved_req">
            <p>Approved</p>
          </div>
        </div>

        <div className="user_notification">
          <div className="notification">
            <div className="notification_detail">
              <div className="notification_img">
                <img src={wazuka_sms_img} alt="" />
              </div>
              <div className="notification_name">
                <h1>Wazuka Inoguchi</h1>
                <p>
                  Canceled By: <span> Nurse Shumaila</span>
                </p>
                <h6>Yesterday</h6>
              </div>
            </div>
          </div>
          <div className="cancel_req">
            <p>Canceled</p>
          </div>
        </div>

        <div className="user_notification">
          <div className="notification">
            <div className="notification_detail">
              <div className="notification_img">
                <img src={tatiana_sms_img} alt="" />
              </div>
              <div className="notification_name">
                <h1>Tatiana Kozlov</h1>
                <p>
                  Blocked By: <span> Doctor Ch Under Taker</span>
                </p>
                <h6>March 23, 2021</h6>
              </div>
            </div>
          </div>
          <div className="block_req">
            <p>Blocked</p>
          </div>
        </div>

        <div className="user_notification">
          <div className="notification">
            <div className="notification_detail">
              <div className="notification_img">
                <img src={doston_sms_img} alt="" />
              </div>
              <div className="notification_name">
                <h1>Eli Dotson</h1>
                <p>New update available for you</p>
                <h6>May 6, 2021</h6>
              </div>
            </div>
          </div>
          <div className="update_button">
            <button type="button" className="update_btn">
              Update Now
            </button>
          </div>
        </div>

        <div className="user_notification">
          <div className="notification">
            <div className="notification_detail">
              <div className="notification_img">
                <img src={wolf_sms_img} alt="" />
              </div>
              <div className="notification_name">
                <h1>Juniper Wolf</h1>
                <p>I need a new appointment</p>
                <h6>August 2, 2021</h6>
              </div>
            </div>
          </div>
          <div className="see_chat_button">
            <button type="button" className="see_chat_btn">
              See Chat
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
