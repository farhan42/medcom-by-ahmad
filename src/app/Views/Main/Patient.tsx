import React, { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import { NavLink } from "react-router-dom";

import patient_menu from "../../../assets/icons/patient_menu.svg";
import pending_requests_icon from "../../../assets/icons/pending_requests.svg";
import { useDispatch } from "../../../store";
import { PatientListThunk } from "../../../thunks";
import { BroadcastDialog } from "../../Components";

const patient_img_1 = require("../../../assets/Images/patient_img_1.png");

export const Patient = (): JSX.Element => {
  const [broadcastMessageDialog, setBroadcastMessageDialog] =
    useState<boolean>(false);

  const hanldeShowBroadcastMessageDialog = () => {
    setBroadcastMessageDialog(true);
  };

  const handleCloseBroadcastMessageDialog = () => {
    setBroadcastMessageDialog(false);
  };

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(PatientListThunk());
  }, []);

  return (
    <>
      <div className="our_patients">
        <div className="patients_container">
          <div className="patients_head">
            <div className="patients_name">
              <h1>Patients</h1>
            </div>
            <div className="patients_btns">
              <button
                className="broadcast"
                onClick={hanldeShowBroadcastMessageDialog}
              > Broadcast </button>
              <NavLink to="/pending_requests" className="pending_req">
                Pending Request <img src={pending_requests_icon} alt="" />
              </NavLink>
              <button className="new_patients">Add New Patient</button>
            </div>
          </div>

          <div className="patients_list">
            <table>
              <tr>
                <th>
                  {" "}
                  <input type="checkbox" />{" "}
                </th>
                <th>Patient Name</th>
                <th>Email</th>
                <th>Product ID</th>
                <th>Insurance</th>
                <th>Phone Number</th>
                <th>Address</th>
                <th></th>
              </tr>
              <tr>
                <td>
                  <input type="checkbox" />
                </td>
                <td className="gap">
                  <img src={patient_img_1} alt="" />
                  <tr>Tatiana Kozlov</tr>
                </td>
                <td> danghoang87hl@gmail.com </td>
                <td> 283785 </td>
                <td> 283785 </td>
                <td> (201) 555-0124 </td>
                <td> 1 Byworth Close, Brighton, </td>
                <td>
                  <div className="menu">
                    <Dropdown align="end">
                      <Dropdown.Toggle className="patient_menu">
                        {" "}
                        <img src={patient_menu} alt="" />{" "}
                      </Dropdown.Toggle>
                      <Dropdown.Menu className="p_dropdown_menu">
                        <Dropdown.Item className="send_sms">
                          {" "}
                          Send Message{" "}
                        </Dropdown.Item>
                        <Dropdown.Item className="view_prof">
                          {" "}
                          View Profile{" "}
                        </Dropdown.Item>
                        <Dropdown.Item className="block"> Block </Dropdown.Item>
                        <div className="patient_top_face"></div>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <BroadcastDialog
        show={broadcastMessageDialog}
        onClose={handleCloseBroadcastMessageDialog}
      />
    </>
  );
};
