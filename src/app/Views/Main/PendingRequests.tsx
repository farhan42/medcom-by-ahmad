import React, { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import { BroadcastDialog, PendingResquestsDialog } from "../../Components";

import patient_menu from "../../../assets/icons/patient_menu.svg";
import back_aero from "../../../assets/icons/back.svg";
import { useNavigate } from "react-router";
import { PendingRequestThunck } from "../../../thunks";
import { useDispatch } from "../../../store";
import { useAuthContext } from "../../contexts";
const doston_sms_img = require("../../../assets/Images/doston_1.png");

export const PendingRequests = (): JSX.Element => {
  const [broadcastMessageDialog, setBroadcastMessageDialog] =
    useState<boolean>(false);
  const [pendingResquestsDialog, setPendingResquestsDialog] =
    useState<boolean>(false);

  const history = useNavigate();

  const hanldeShowBroadcastMessageDialog = () => {
    setBroadcastMessageDialog(true);
  };

  const handleCloseBroadcastMessageDialog = () => {
    setBroadcastMessageDialog(false);
  };

  const handleShowPendingRequestDialog = () => {
    setPendingResquestsDialog(true);
  };

  const handleClosePendingResquestsDialog = () => {
    setPendingResquestsDialog(false);
  };

  const context = useAuthContext();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(PendingRequestThunck([{ clinicID: context.user.clinicID }]));
  });

  return (
    <>
      <div className="our_patients my_pending_requests">
        <div className="patients_container pending_requests_container">
          <div className="patients_head pending_requests_head">
            <div className="patients_name pending_requests_name">
              <img onClick={() => history(-1)} src={back_aero} alt="" />
              <h1>Pending Request</h1>
            </div>
            <div className="patients_btns pending_requests_broadcasr_btn">
              <button
                className="broadcast"
                onClick={hanldeShowBroadcastMessageDialog}
              >
                {" "}
                Broadcast{" "}
              </button>
            </div>
          </div>

          <div className="patients_list pending_requests_list">
            <table>
              <tr>
                <th>
                  <input type="checkbox" />
                </th>
                <th>Patient Name</th>
                <th>Email</th>
                <th>Product ID</th>
                <th>Insurance</th>
                <th>Phone Number</th>
                <th>Address</th>
                <th></th>
              </tr>
              <tr>
                <td>
                  <input type="checkbox" />
                </td>
                <td className="gap">
                  <img
                    onClick={handleShowPendingRequestDialog}
                    src={doston_sms_img}
                    alt=""
                  />
                  <tr>Eli Dotson</tr>
                </td>
                <td> tienlapspktnd@gmail.com </td>
                <td> 183685 </td>
                <td> 183685 </td>
                <td> (480) 555-0103 </td>
                <td> 3890 Poplar Dr. </td>
                <td>
                  <div className="menu pending_requests_menu">
                    <Dropdown align="end">
                      <Dropdown.Toggle className="patient_menu pending_menu">
                        {" "}
                        <img src={patient_menu} alt="" />{" "}
                      </Dropdown.Toggle>
                      <Dropdown.Menu className="p_r_dropdown_menu">
                        <Dropdown.Item className="approve">
                          {" "}
                          Approve{" "}
                        </Dropdown.Item>
                        <Dropdown.Item className="cancel">
                          {" "}
                          Cancel{" "}
                        </Dropdown.Item>
                        <Dropdown.Item className="block"> Block </Dropdown.Item>
                        <div className="p_r_top_face"></div>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <BroadcastDialog
        show={broadcastMessageDialog}
        onClose={handleCloseBroadcastMessageDialog}
      />

      <PendingResquestsDialog
        show={pendingResquestsDialog}
        onClose={handleClosePendingResquestsDialog}
      />
    </>
  );
};
