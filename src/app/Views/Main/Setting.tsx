import { useFormik } from "formik";
import React, { ChangeEvent, useEffect, useState } from "react";
import { API } from "../../../api";

import Edit_profile_camera_icon from "../../../assets/icons/camera_icon.svg";
import tag_cross from "../../../assets/icons/tag_remove_cross.svg";
import { ClinicInterface } from "../../../interfaces";
import { FormikValidator } from "../../../shared/utility";
import { SettingValidator } from "../../../Validators";
import {
  ChangeThemeColorDialog,
  ClinicLocationDialog,
  FormikErrorMessage,
} from "../../Components";
import { useAuthContext } from "../../contexts";

// const edit_profile_img = require("../../../assets/Images/Edit_profile.png");
const clinic_c_location = require("../../../assets/Images/clinic_location.png");
const map_cur_handle = require("../../../assets/Images/map_handle.png");

export const Setting = (): JSX.Element => {
  const context = useAuthContext();
  const [currentLocation, setCurrentLocation] = useState<boolean>(false);
  const [changeThemeColor, setChangeThemeColor] = useState<boolean>(false);

  const handleShowCurrentLocation = () => {
    setCurrentLocation(true);
  };

  const handleCloseCurrentLocation = () => {
    setCurrentLocation(false);
  };

  const handleShowChangeThemeColor = () => {
    setChangeThemeColor(true);
  };

  const handleCloseChangeThemeColor = () => {
    setChangeThemeColor(false);
  };

  const [tags, setTags] = useState([]);

  const removeTags = (indexToRemove) => {
    setTags(tags.filter((_, index) => index != indexToRemove));
  };

  const addTags = (event) => {
    if (event.key === "Enter") {
      setTags([...tags, event.target.value]);
      event.target.value = "";
    }
  };

  const settingFormik = useFormik<SettingValidator>({
    initialValues: {
      clinicname: "",
      password: "",
      description: "",
      email: context.user.email,
      phoneNumber: context.user.phoneNumber,
      tags: "",
    },
    onSubmit: (value) => {
      API.ClinicUpdate.updateById(context.user.id, value)
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          console.log(error);
        });
    },
    validate: FormikValidator.validator(SettingValidator),
  });

  const [images, setImages] = useState<string[]>([]);
  const handleChangeFile = (event: ChangeEvent<HTMLInputElement>) => {
    const target = event.currentTarget;
    const { files } = target;
    if (files.length) {
      const file = files[0];
      API.Media.upload(file)
        .then((response) => {
          setImages((state) => [
            ...state,
            API.Media.getFileByName(response.data.filename),
          ]);
        })
        .catch(console.log);
    }
  };

  useEffect(() => {
    console.log("User from settings =>", context.user.id);
  }, []);
  return (
    <>
      <div className="setting">
        <div className="setting_container">
          <div className="setting_head">
            <h1> Edit Profile</h1>
            <button
              type="button"
              className="c_change"
              onClick={handleShowChangeThemeColor}
            >
              Change Colors
            </button>
          </div>

          <div className="flex">
            <form
              onSubmit={settingFormik.handleSubmit}
              className="setting_input_fields"
            >
              <div className="setting_left_input_fields">
                <div className="setting_img">
                  <label htmlFor="file" className="file_label">
                    <img
                      src={Edit_profile_camera_icon}
                      className="camera"
                      alt=""
                    />
                    <input
                      type="file"
                      name="file"
                      id="file"
                      hidden
                      onChange={handleChangeFile}
                    />
                  </label>
                  {/* <img src={add_hospital_img} alt="" /> */}
                </div>

                <div className="images">
                  {images.map((image, index) => (
                    <div className="added_img" key={index}>
                      <img src={image} alt="" className="file" />
                    </div>
                  ))}
                </div>

                <div className="setting_left_main_input_fields">
                  <div className="setting_left_input_field">
                    <label htmlFor="clinicname"> Clinic Name </label>
                    <input
                      name="clinicname"
                      id="clinicname"
                      type="text"
                      className="input"
                      placeholder="clinic Name"
                      onChange={settingFormik.handleChange}
                      onBlur={settingFormik.handleBlur}
                    />
                    <FormikErrorMessage
                      formik={settingFormik}
                      name="clinicname"
                      render={(error) => <span className="error">{error}</span>}
                    />
                  </div>

                  <div className="setting_left_input_field">
                    <label htmlFor="password"> Password </label>
                    <input
                      name="password"
                      id="password"
                      type="password"
                      className="input"
                      placeholder="Password"
                      onChange={settingFormik.handleChange}
                      onBlur={settingFormik.handleBlur}
                    />
                    <FormikErrorMessage
                      formik={settingFormik}
                      name="password"
                      render={(error) => <span className="error">{error}</span>}
                    />
                  </div>

                  <div className="setting_left_input_field">
                    <label htmlFor="description"> Description </label>
                    <textarea
                      name="description"
                      id="description"
                      rows={6}
                      placeholder="Description"
                      onChange={settingFormik.handleChange}
                      onBlur={settingFormik.handleBlur}
                    ></textarea>
                    <FormikErrorMessage
                      formik={settingFormik}
                      name="description"
                      render={(error) => <span className="error">{error}</span>}
                    />
                  </div>
                  <div className="setting_save_btn">
                    <button type="submit" className="save_btn">
                      Save
                    </button>
                  </div>
                </div>
              </div>
              <div className="setting_right_input_fields">
                <div className="setting_right_input_field">
                  <label htmlFor="email"> Email Address </label>
                  <input
                    name="email"
                    id="email"
                    type="email"
                    className="input"
                    placeholder="Email Address"
                    value={context.user.email}
                    onChange={settingFormik.handleChange}
                    onBlur={settingFormik.handleBlur}
                  />
                  <FormikErrorMessage
                    formik={settingFormik}
                    name="email"
                    render={(error) => <span className="error">{error}</span>}
                  />
                </div>

                <div className="setting_right_input_field">
                  <label htmlFor="phoneNumber"> Phone Number </label>
                  <input
                    name="phoneNumber"
                    id="phoneNumber"
                    type="tel"
                    className="input"
                    placeholder="Phone Number"
                    value={context.user.phoneNumber}
                    onChange={settingFormik.handleChange}
                    onBlur={settingFormik.handleBlur}
                  />
                  <FormikErrorMessage
                    formik={settingFormik}
                    name="address"
                    render={(error) => <span className="error">{error}</span>}
                  />
                </div>
                <div className="setting_right_input_field">
                  <label htmlFor="tags"> Tags </label>
                  <div className="tags_input">
                    <input
                      type="text"
                      name="tags"
                      id="tags"
                      className="input"
                      placeholder="Add tags"
                      onKeyUp={addTags}
                      onChange={settingFormik.handleChange}
                      onBlur={settingFormik.handleBlur}
                    />
                    <div className="points">
                      {tags.map((tag, index) => (
                        <p key={index}>
                          <span>{tag}</span>
                          <img
                            src={tag_cross}
                            alt=""
                            onClick={() => removeTags(index)}
                          />
                        </p>
                      ))}
                    </div>
                  </div>
                  <FormikErrorMessage
                    formik={settingFormik}
                    name="tags"
                    render={(error) => <span className="error">{error}</span>}
                  />
                </div>
              </div>
            </form>

            <div className="c_location">
              <div className="c_c_location">
                <img src={clinic_c_location} alt="" />
                <div className="map_numaric">
                  <p className="numaric">31.505726 74.274032</p>
                  <p className="down"></p>
                </div>
                <div className="map_cur_handle">
                  <img src={map_cur_handle} alt="" />
                </div>
                <div className="zooming">
                  <button className="zoom_in" type="button">
                    +
                  </button>
                  <button className="zoom_out" type="button">
                    -
                  </button>
                </div>
              </div>
              <div className="c_c_btn">
                <button
                  type="button"
                  className="c_btn"
                  onClick={handleShowCurrentLocation}
                >
                  {" "}
                  Change Your Clinic Location{" "}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <ClinicLocationDialog
        show={currentLocation}
        onClose={handleCloseCurrentLocation}
      />

      <ChangeThemeColorDialog
        show={changeThemeColor}
        onClose={handleCloseChangeThemeColor}
      />
    </>
  );
};
