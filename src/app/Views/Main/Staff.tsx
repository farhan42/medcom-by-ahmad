import React, { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";

import patient_menu from "../../../assets/icons/patient_menu.svg";
import { UserInterface } from "../../../interfaces";
import { useDispatch, useSelector } from "../../../store";
import { ClinicStaffThunk } from "../../../thunks/clinic-staff-thunk";
import {
  AddNewStaffDialog,
  DeleteStaffDialog,
  EditStaffDialog,
} from "../../Components";
import { useAuthContext } from "../../contexts";

const patient_img_1 = require("../../../assets/Images/patient_img_1.png");

export const Staff = (): JSX.Element => {
  const [addStaffDialog, setAddStaffDialog] = useState<boolean>(false);
  const [editStaffDialog, setEditStaffDialog] = useState<UserInterface>(null);
  const [deleteStaffDialog, setDeleteStaffDialog] = useState<string>(null);

  const handleShowAddStaffDialog = () => {
    setAddStaffDialog(true);
  };

  const handleCloseAddStaffDialog = () => {
    setAddStaffDialog(false);
  };

  const handleShowEditStaffDialog = (user: UserInterface) => () => {
    setEditStaffDialog(user);
  };

  const handleCloseEditStaffDialog = () => {
    setEditStaffDialog(null);
  };

  const hanldeShowDeleteDialog = (id: string) => () => {
    setDeleteStaffDialog(id);
  };

  const handleCloseDeleteDialog = () => {
    setDeleteStaffDialog(null);
  };

  const context = useAuthContext();
  const dispatch = useDispatch();
  const staff = useSelector((state) => state.clinicStaff);
  useEffect(() => {
    dispatch(ClinicStaffThunk([{ clinicID: context.user.clinicID }]));
  }, []);

  return (
    <>
      <div className="our_staff">
        <div className="staff_container">
          <div className="staff_head">
            <div className="staff_name">
              <h1>Staff</h1>
            </div>
            <div className="staff_buttons">
              <button className="message">Message</button>
              <button className="add_staff" onClick={handleShowAddStaffDialog}>
                Add Staff
              </button>
            </div>
          </div>
          <div className="staff_users">
            <table>
              <tr>
                <th>
                  <input type="checkbox" />
                </th>
                <th>Staff Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>PHone Number</th>
                <th>Address</th>
                <th></th>
              </tr>
              {staff.data.map((item) => (
                <tr key={item.id}>
                  <td>
                    <input type="checkbox" />
                  </td>
                  <td className="gap">
                    <img src={item.avatar || patient_img_1} alt="" />
                    <td> {item.firstName} </td>
                  </td>
                  <td> {item.email} </td>
                  <td> {item.role} </td>
                  <td> {item.phoneNumber} </td>
                  <td> {item.address} </td>
                  <td>
                    <div className="menu">
                      <Dropdown align="end">
                        <Dropdown.Toggle className="patient_menu">
                          {" "}
                          <img src={patient_menu} alt="" />{" "}
                        </Dropdown.Toggle>
                        <Dropdown.Menu className="dropdown_menu">
                          <Dropdown.Item
                            className="edit"
                            onClick={handleShowEditStaffDialog(item)}
                          >
                            Edit
                          </Dropdown.Item>
                          <Dropdown.Item
                            className="delete"
                            onClick={hanldeShowDeleteDialog(item.id)}
                          >
                            {" "}
                            Delete{" "}
                          </Dropdown.Item>
                          <div className="staff_top_face"></div>
                        </Dropdown.Menu>
                      </Dropdown>
                    </div>
                  </td>
                </tr>
              ))}
            </table>
          </div>
        </div>
      </div>

      <AddNewStaffDialog
        show={addStaffDialog}
        onClose={handleCloseAddStaffDialog}
      />

      {!!editStaffDialog && (
        <EditStaffDialog
          show={!!editStaffDialog}
          user={editStaffDialog}
          onClose={handleCloseEditStaffDialog}
        />
      )}

      {!!deleteStaffDialog && (
        <DeleteStaffDialog
          show={!!deleteStaffDialog}
          id={deleteStaffDialog}
          onClose={handleCloseDeleteDialog}
        />
      )}
    </>
  );
};
