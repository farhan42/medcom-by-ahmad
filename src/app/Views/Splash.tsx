import React from 'react'

const splash_image = require('../../assets/Images/splash_img.png');

export const Splash = ():JSX.Element => {
  return (
    <div className='splash_container'>
        <div className="splash_img">
            <img src={splash_image} alt="" />
        </div>
    </div>
  )
}
