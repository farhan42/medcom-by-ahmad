import React from "react";
import { Outlet } from "react-router-dom";

export const Authlayout = () => {
  return (
    <div className="container_c">
      <div className="authlayout">
        <Outlet />
      </div>
    </div>
  );
};
