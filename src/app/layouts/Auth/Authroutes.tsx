import React from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Register, Login } from "../../Views/Auth";
import { Authlayout } from "./Authlayout";

export const AuthRoutes = (): JSX.Element => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Authlayout />}>
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/" element={<Navigate to="/login" />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
