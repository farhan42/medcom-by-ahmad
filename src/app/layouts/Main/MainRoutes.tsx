import React from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Home, Notification, Patient, PendingRequests, Setting, Staff } from "../../Views/Main";
import { MainChat } from "../../Views/Main/Chat";
import { Mainlayout } from "./Mainlayout";

export const MainRoutes = () => {
  return (
    <div className="mainroutes">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Mainlayout />}>
            <Route path="/home" element={<Home />} />
            <Route path="/staff" element={<Staff />} />
            <Route path="/patients" element={<Patient />} />
            <Route path="/chat" element={<MainChat />} />
            <Route path="/notification" element={<Notification />} />
            <Route path="/settings" element={<Setting />} />
            <Route path="/pending_requests" element={<PendingRequests />} />
            <Route path="/all_notification" element={<Notification />} />
            <Route path="/" element={<Navigate to="/home" />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
};
