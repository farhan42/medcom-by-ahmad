import React from "react";
import { Outlet } from "react-router-dom";
import { Header, Sidebar } from "../../Components";

export const Mainlayout = (): JSX.Element => {
  return (
    <div className="mainlayout">
      <div className="mainlayout_container">
        <div className="sidebar">
          <Sidebar />
        </div>
        <div className="outlet">
          <Header />
          <Outlet />
        </div>
      </div>
    </div>
  );
};
