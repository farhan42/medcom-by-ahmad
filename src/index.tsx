import React from "react";
import ReactDOM from "react-dom/client";
import { App } from "./App";
import { AuthContext, AuthContextProvider } from "./app/contexts";
import "./assets/scss/index.css";
import { Provider } from "react-redux";
import store from "./store";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <AuthContextProvider>
    <Provider store={store}>
      <App />
    </Provider>
  </AuthContextProvider>
);
