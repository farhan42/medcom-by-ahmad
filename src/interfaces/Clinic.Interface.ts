interface LocationEnum {
  type: string;
  coordinates: [number, number];
}

interface AddressEnum {
  locality: string;
  location: LocationEnum;
}

export interface ClinicInterface {
  id: string,
  owner: string,
  title: string,
  description: string,
  email: string,
  emailVerified: false,
  phoneNumber: string,
  phoneNumberVerified: false,
  address: AddressEnum;
  logo: string;
  tags: string[];
  colors: {};
}
