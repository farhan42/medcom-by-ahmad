export interface MediaUploadInterface {
  path: string;
  filename: string;
}
