export interface MyNotificationInterface {
  statusCode: 0;
  message: string;
  error: string;
}
