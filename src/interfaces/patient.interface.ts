export interface PatientEmbeddedInterface {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  dob: string,
  phoneNumber: string;
  address: string;
  avatar: string;
  deleted: false;
}

export interface PatientInteface {
  id: string;
  patientId: string;
  clinicId: string;
  existingId: string;
  insuranceId: string;
  baseRoom: string;
  actionBy: string;
  remark: string;
  verified: null;
  blocked: false;
  deleted: false;
  patientEmbedded: PatientEmbeddedInterface;
}
