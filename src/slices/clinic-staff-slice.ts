import { ClinicStaffThunk } from './../thunks/clinic-staff-thunk';
import { createSlice } from "@reduxjs/toolkit";
import { UserInterface } from '../interfaces';

interface ClinicStaffState {
  data: UserInterface[];
  loading: boolean;
  error: any;
}

const initialState: ClinicStaffState = {
  data: [],
  loading: false,
  error: null,
};

export const ClinicStaffSlice = createSlice({
  name: "clinic-staff-slice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // Clinics
    builder
      .addCase(ClinicStaffThunk.pending, (state) => {
        state.loading = !state.data.length;
        state.error = null;
      })
      .addCase(ClinicStaffThunk.fulfilled, (state, { payload }) => {
        if (payload) {
          if (payload.length) {
            if (payload[0].user) {
              state.data = payload[0].user.filter((item) => !item.deleted);
            }
          }
        }
        state.loading = false;
        state.error = null;
      })
      .addCase(ClinicStaffThunk.rejected, (state, action) => {
        if (action.payload) {
          state.error = action.payload;
        } else {
          state.error = action.error;
        }
        state.loading = false;
      });
  },
});
