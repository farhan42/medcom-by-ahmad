import { createSlice } from "@reduxjs/toolkit";
import { PatientInteface } from "../interfaces";
import { deleteStaffThunk } from "../thunks";

interface DeleteStaffState {
  data: PatientInteface[];
  loading: boolean;
  error: any;
}

const initialState: DeleteStaffState = {
  data: [],
  loading: false,
  error: null,
};

export const DeleteStaffSlice = createSlice({
  name: "delete-staff-slice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // Delete staff
    builder
      .addCase(deleteStaffThunk.pending, (state) => {
        state.loading = !state.data.length;
        state.error = null;
      })
      .addCase(deleteStaffThunk.fulfilled, (state, { payload }) => {
        // state.data = payload;
        state.loading = false;
        state.error = null;
      })
      .addCase(deleteStaffThunk.rejected, (state, action) => {
        if (action.payload) {
          state.error = action.payload;
        } else {
          state.error = action.error;
        }
        state.loading = false;
      });
  },
});
