import { createSlice } from "@reduxjs/toolkit";
import { MyNotificationInterface } from "../interfaces";
import { NotificationListThunk } from "../thunks";

interface NotificationReadListState {
  data: MyNotificationInterface[];
  loading: boolean;
  error: any;
}

const initialState: NotificationReadListState = {
  data: [],
  loading: false,
  error: null,
};

export const MyNotificationListSlice = createSlice({
  name: "notification-read-slice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // Notification Read List
    builder
      .addCase(NotificationListThunk.pending, (state) => {
        state.loading = !state.data.length;
        state.error = null;
      })
      .addCase(NotificationListThunk.fulfilled, (state, { payload }) => {
        // state.data = payload;
        state.loading = false;
        state.error = null;
      })
      .addCase(NotificationListThunk.rejected, (state, action) => {
        if (action.payload) {
          state.error = action.payload;
        } else {
          state.error = action.error;
        }
        state.loading = false;
      });
  },
});
