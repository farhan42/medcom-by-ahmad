import { createSlice } from "@reduxjs/toolkit";
import { PatientInteface } from "../interfaces";
import { PatientListThunk } from "../thunks/patient-list-thunk";

interface PatientListState {
  data: PatientInteface[];
  loading: boolean;
  error: any;
}

const initialState: PatientListState = {
  data: [],
  loading: false,
  error: null,
};

export const PatientListSlice = createSlice({
  name: "patient-slice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // Patient List
    builder
      .addCase(PatientListThunk.pending, (state) => {
        state.loading = !state.data.length;
        state.error = null;
      })
      .addCase(PatientListThunk.fulfilled, (state, { payload }) => {
        // state.data = payload;
        state.loading = false;
        state.error = null;
      })
      .addCase(PatientListThunk.rejected, (state, action) => {
        if (action.payload) {
          state.error = action.payload;
        } else {
          state.error = action.error;
        }
        state.loading = false;
      });
  },
});
