import { MyNotificationListSlice } from "../slices/notification-slice";
import { combineReducers } from "@reduxjs/toolkit";
import {
  ClinicStaffSlice,
  DeleteStaffSlice,
  PatientListSlice,
} from "../slices";

export const rootReducer = combineReducers({
  clinicStaff: ClinicStaffSlice.reducer,
  patientList: PatientListSlice.reducer,
  notificationList: MyNotificationListSlice.reducer,
  deleteStaff: DeleteStaffSlice.reducer,
});
