import { UserFilterInterface } from "./../interfaces";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "../api";
import { UserInterface } from "../interfaces";

export const ClinicStaffThunk = createAsyncThunk(
  "clinic-staff-thunk",
  async (filter: UserFilterInterface[], thunkApi: any) => {
    try {
      const response = await API.ClinicStaff.list({ filter: filter });
      return response.data;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.response);
    }
  }
);

export const deleteStaffThunk = createAsyncThunk<UserInterface, string>(
  "delete-staff-thunk",
  async (id, thunkApi) => {
    try {
      const response = await API.ClinicStaff.deleteById(id);
      return response.data;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.response);
    }
  }
);
