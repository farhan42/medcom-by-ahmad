import { API } from "../api/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
export const NotificationListThunk = createAsyncThunk(
  "my-notification-list-thunk",
  async (data, thunkApi) => {
    try {
      const response = await API.Notification.list({ data });
      return response.data;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.response);
    }
  }
);
