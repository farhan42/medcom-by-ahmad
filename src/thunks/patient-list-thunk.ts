import { createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "../api";
import { PatientInteface } from "../interfaces";
// import { PatientInteface } from "../interfaces";

export const PatientListThunk = createAsyncThunk(
  "patient-list-thunk",
  async (data, thunkApi) => {
    try {
      const response = await API.Patients.list();
      return response.data;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.response);
    }
  }
);

export const DeletePatientThunk = createAsyncThunk<PatientInteface, string>(
  "delete-patient-thunk",
  async (id, thunkApi) => {
    try {
      const response = await API.Patients.deleteById(id);
      return response.data;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.response);
    }
  }
);
