import { UserFilterInterface } from "./../interfaces/user.interface";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "../api";
export const PendingRequestThunck = createAsyncThunk(
  "pending-request-thunk",
  async (pfilter: UserFilterInterface[], thunkApi: any) => {
    try {
      const response = await API.Patients.filter({ pfilter: pfilter });
      return response.data;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.response);
    }
  }
);
